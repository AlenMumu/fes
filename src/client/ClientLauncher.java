package client;

import packets.Packet99ChatMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by amujkic on 23.04.2014.
 */
public class ClientLauncher extends JFrame {


    private JPanel gui = new JPanel(new BorderLayout());
    private JPanel chatContainer = new JPanel(new BorderLayout());
    private JPanel typeAreaContainer = new JPanel(new BorderLayout());
    private JTextArea console = new JTextArea();
    private JTextArea typeArea = new JTextArea();
    private JButton sendButton = new JButton("Send");
    private JLabel userName = new JLabel("");
   private JScrollPane scrollPane;
private  Screen screen;
private Menu menu;
    public ClientLauncher() {
        setSize(new Dimension(910, 693));
        setTitle("Game");
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        add(gui, BorderLayout.CENTER);
        gui.setVisible(true);
        gui.setSize(getSize());
        chatContainer.setVisible(true);
        chatContainer.setSize(910, 493);
userName.setSize(new Dimension(100,15));
        scrollPane = new JScrollPane(console);
console.setBackground(Color.BLACK);
console.setForeground(Color.WHITE);
        typeAreaContainer.add(typeArea,BorderLayout.CENTER);

        typeAreaContainer.add(userName,BorderLayout.WEST);
        chatContainer.add(typeAreaContainer,BorderLayout.NORTH);
        chatContainer.add(scrollPane, BorderLayout.CENTER);
        chatContainer.add(sendButton,BorderLayout.EAST);
        gui.add(chatContainer, BorderLayout.SOUTH);



pack();



    }

    public void joinRoom(){
                screen = new Screen(1200, 700, JOptionPane.showInputDialog(this, "Please enter the IP address of the server..."), this);


        gui.add(screen, BorderLayout.CENTER);

        final Thread screenThread = new Thread(screen);
        screenThread.start();
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!typeArea.getText().isEmpty()){
                    Date date = new Date();
                    System.out.println("SENT"+date.toString()+userName.getText()+typeArea.getText());
                    screen.sendDataToServer(new Packet99ChatMessage(date.toString(),userName.getText(),typeArea.getText()));
                }
            }
        });
        pack();
        screen.doWork();
    }

    public void setUserName(String userNameText){
        userName.setText(userNameText);
    }

    public JTextArea getConsole() {
        return console;
    }



    public JPanel getGui() {
        return gui;
    }

    public void setGui(JPanel gui) {
        this.gui = gui;
    }
}
