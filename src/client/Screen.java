package client;

import entities.Character;
import entities.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.ResourceLoader;
import packets.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by amujkic on 23.04.2014.
 */


public class Screen extends Canvas implements Runnable {
    /**
     * The fonts to draw to the screen
     */



    int longestString = 0;
    String longString = "";
    java.awt.Font awtFont;
    Font awtFont2;
    private boolean showScore = false;
    final Object animationLock = new Object();
    final Object bloodLock = new Object();
    final Object bulletLock = new Object();
    private volatile TrueTypeFont font;
    private volatile TrueTypeFont font2;
    private volatile TiledMap map;
    private volatile TiledMap characterMap;
    private ArrayList<VisualBullet> bullets = new ArrayList<>();
    private ArrayList<VisualBlood> blood = new ArrayList<>();
    float lookAtX = 0;
    float lookAtY = 0;
VisualBullet grenadeTextureBullet = new VisualBullet("",0,0,"","");
    VisualBullet grenadeShrapnel = new VisualBullet("",0,0,"","");
    VisualBullet normalBullet = new VisualBullet("",0,0,"","");
    VisualBlood bloodSample = new VisualBlood("",0,0,0);

    /**
     * Boolean flag on whether AntiAliasing is enabled or not
     */
   boolean mouseDown = false;


    private float ammo = 100;
    private double delta = 0;
    private int layerX = 0;
    private int layerY = 0;
    private boolean antiAlias = true;
    //private Character entity;
    private int width, height;
    private InetAddress ipAddress;
    private DatagramSocket socket;
    private ArrayList<Entity> players = new ArrayList<>();
    private ArrayList<Weapon> weapons = new ArrayList<>();
    private ArrayList<Weapon> removedWeapons = new ArrayList<>();
    private ArrayList<Packet04Item> itemPackets = new ArrayList<>();
    private Texture weaponTexture;
    private Texture playerTexture;
    private Texture mouseTexture;
    private ClientLauncher launcher;
    private ChatRoom chatRoom;
    private boolean receivedItems = false;
    String userName;

private ArrayList<String> songs = new ArrayList<>();
    private Audio wavEffect;
    private Audio emptyClipEffect;
    private Audio heartBeat;
    private Audio feet;
    private Audio shellCasing;
    private Audio bulletHit;
    private Audio bulletHitDirt;
    private Audio grenadeExplosion;
    private Audio grenadePin;
    private Audio music;
    private Audio backPack;
    private Audio gunCock;
    final Object statusStringLock = new Object();
    private SpriteSheet spriteSheet;
private ArrayList<VisualExplosion> explosions = new ArrayList<>();

    public Screen(int width, int height, String ipAddressName, ClientLauncher launcher) {
        super();
        this.launcher = launcher;
        try {
            this.socket = new DatagramSocket();
            this.ipAddress = InetAddress.getByName(ipAddressName);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.width = width;
        this.height = height;

        userName = JOptionPane.showInputDialog(launcher, "Please enter a username");
        launcher.setUserName(userName);
        // this.entity = new Character(userName, 0, 0, 0);
        this.chatRoom = new ChatRoom(launcher, this);


        setFocusable(true);
        requestFocus();
        setIgnoreRepaint(true);


        sendDataToServer(new Packet00Login(userName, 0, 0, 0));
        setSize(new Dimension(width, height));


        songs.add("Tidal_Wave.wav");
        songs.add("Jazz_In_Paris.wav");
        songs.add("Dub_Spirit.wav");

    }



    public void setupScreen(int width, int height) {


Mouse.setGrabbed(true);
        try {

            Display.setParent(this);
            Display.setDisplayMode(Display.getDesktopDisplayMode());
            Display.setFullscreen(true);
            Display.setVSyncEnabled(true);
            Display.create();


        } catch (LWJGLException e) {
            e.printStackTrace();
        }



        initGL();


        try {
            map = new TiledMap("res/levels/" + "someFeetUnder.tmx");
            characterMap = new TiledMap("res/levels/" + "characterMap.tmx");

        } catch (SlickException e) {
            e.printStackTrace();
        }


        // load a default java font
       awtFont = new Font("Impact", Font.BOLD, 15);
        font = new TrueTypeFont(awtFont, antiAlias);

        // load font from file
        try {
            InputStream inputStream = ResourceLoader.getResourceAsStream("res/birdsFont.ttf");

            awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            awtFont2 = awtFont2.deriveFont(15); // set font size
            font2 = new TrueTypeFont(awtFont2, antiAlias);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream playerStream = new FileInputStream(new File("res/sp-studio.png"));
            playerTexture = TextureLoader.getTexture("PNG", playerStream);
            playerStream.close();

            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            FileInputStream weaponStream = new FileInputStream(new File("res/mp5.png"));
            weaponTexture = TextureLoader.getTexture("PNG", weaponStream);

            weaponStream.close();
            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            FileInputStream mouseStream = new FileInputStream(new File("res/crosshair.png"));
            mouseTexture = TextureLoader.getTexture("PNG", mouseStream);

            mouseStream.close();
            // Replace PNG with your file extension
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }


        try {
            wavEffect = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/m4shot.wav"));
            emptyClipEffect = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/emptyclip.wav"));
            heartBeat = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/heartbeat.wav"));
            feet = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/footstepDirt.wav"));
            shellCasing = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/shellFalling.wav"));
            bulletHit = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/bulletHit.wav"));
            bulletHitDirt = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/bulletDirt.wav"));
            grenadeExplosion = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/explosion(1).wav"));
            grenadePin = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/grenadePinPull.wav"));
            music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/warAmbience1.wav"));
            gunCock = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/gunCock.wav"));
            backPack = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/shuffleBackpack.wav"));


            backPack.playAsSoundEffect(1f,1f,false);

            gunCock.playAsSoundEffect(1f,1f,false);
            music.playAsMusic(1f,0.6f,false);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            spriteSheet = new SpriteSheet("res/images/tiles/spritesheetvolt_run.png",280,385, Color.transparent);
//
//        } catch (SlickException e) {
//            e.printStackTrace();
//        }

//        for(Entity player : players){
//            if(player instanceof Character){
//                try {
//if(((Character) player).getWeapon().getSprite()==null) {
//    SpriteSheet sprite = new SpriteSheet("res/" + ((Character) player).getWeapon().getTextureName(), 100, 100);
//    ((Character) player).getWeapon().setSprite(sprite);
//}
//                } catch (SlickException e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//        }


    }

    double deltaY;
    double deltaX;
    public float mouseDownTime = 0;

    public void userInput(double delta) {

        this.delta = delta;

        int direction = 1;


        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {


           System.exit(0);

        }

        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {


          showScore=true;

        }else{

            showScore=false;
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
            for (Entity entity : players) {


                if (entity.getName().equals(userName)) {

                    sendDataToServer(new Packet05RequestPickup(entity.getX(), entity.getY(), "", entity.getName()));
                    break;
                }
            }
        }


        for (Entity entity : players) {


            if (entity.getName().equals(userName)) {
//            System.out.println(Mouse.getX());
//            System.out.println(lookAtX);
//            System.out.println(entity.getX());
                float angleXCheck = entity.getX();
                if (lookAtX > 0) {
                    angleXCheck = Display.getWidth() / 2;
                } else {
                    angleXCheck = entity.getX();
                }

                if (Mouse.getX() < angleXCheck) {
                    direction = 1;
                    // System.out.println("LESS THAN 0");
                } else {
                    direction = -1;
                }


                float lookAtX = 0;
                float lookAtY = 0;

                if (entity.getX() >= Display.getWidth() / 2) {

                    lookAtX = (Display.getWidth() / 2);

                } else if (entity.getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                    lookAtX = entity.getX();

                }
                if (entity.getY() >= Display.getHeight() / 2) {

                    lookAtY = (Display.getHeight() / 2);

                } else if (entity.getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {

                    lookAtY = entity.getY();


                }


                deltaX = (direction * Mouse.getX()) - (direction * lookAtX);


                if (direction == -1) {
                    deltaY = (-direction * Mouse.getY()) - (-direction * lookAtY);


                } else {
                    deltaY = (direction * Mouse.getY()) - (direction * lookAtY);


                }


                if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {


                    sendDataToServer(new Packet02Move(userName, 0, 1, direction, 0, "jumping",-1,-1));
                }

                if (Keyboard.isKeyDown(Keyboard.KEY_D)) {

                    synchronized (stateLock) {
                    if(!((Character) entity).isJumping()) {

                            ((Character) entity).setState("moving");
                        }
                    }
                    sendDataToServer(new Packet02Move(userName, 1, 0, direction, 0, "moving",-1,-1));
                } else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
                    synchronized (stateLock) {
                    if(!((Character) entity).isJumping()) {

                            ((Character) entity).setState("moving");
                        }
                    }
                    sendDataToServer(new Packet02Move(userName, -1, 0, direction, 0, "moving",-1,-1));
                } else {
                    synchronized (stateLock) {
                    if(!((Character) entity).isJumping()) {

                            ((Character) entity).setState("standing");
                        }
                    }
                    sendDataToServer(new Packet02Move(userName, 0, 0, direction, 0, "standing",-1,-1));
                }
                break;
            }
        }


        if (Mouse.isButtonDown(0)) {


            sendDataToServer(new Packet06Shoot(userName,"weapon"));
sendDataToServer(new Packet14Mouse(0,userName));


//        if(mouseDownTime>=delta*5){
//            wavEffect.playAsSoundEffect(1f,1f,false);
//
////            System.out.println("SHOT");
////            System.out.println("DOWN TIME"+mouseDownTime);
////            System.out.println(delta);
//            mouseDownTime = 0;
//
//
//
//        }else{
//           mouseDownTime+=delta;
////            System.out.println("DOWN TIME"+mouseDownTime);
////            System.out.println(delta);
//        }
            Random r = new Random();

            if (r.nextInt(1) == 0) {


                sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) deltaY + 30 + r.nextInt(50), 0));


            } else {


                sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) ((deltaY + 30 + r.nextInt(50)) * -1), 0));

            }


        } else if(!Mouse.isButtonDown(0)&&!Mouse.isButtonDown(1)) {


            sendDataToServer(new Packet03WeaponAngle(userName, (float) deltaX, (float) deltaY, 0));


            sendDataToServer(new Packet14Mouse(-1,userName));
mouseDown = false;

            for(Entity entity :players){
if(entity.getName().equals(userName)){
    synchronized (stateLock) {
        Character player = (Character) entity;
        player.setShooting(false);
    }
}


            }

        }


        if(Mouse.isButtonDown(1)&&!mouseDown){

            sendDataToServer(new Packet06Shoot(userName,"grenade"));

sendDataToServer(new Packet14Mouse(1,userName));
            mouseDown = true;

        }

//        System.out.println("DELTA "+delta);


    }


    public void sendDataToServer(Packet packet) {

        try {
            socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ipAddress, 1331));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void update(double delta) {


        if(music!=null){

            if(!music.isPlaying()){

                try {
                    music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/"+songs.get(new Random().nextInt(songs.size()-1))));
                    music.playAsMusic(1f,0.4f,false);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        }

        synchronized (animationLock) {
            for (VisualExplosion explosion : explosions) {
                if (explosion.getAnimation() != null) {
                    if(explosion.getAnimation().isStopped()){
                        explosion.getAnimation().start();
                    }
                    explosion.getAnimation().update((long) delta);
                }
            }
        }


        userInput(delta);
        Display.update();
        Display.sync(60);

    }

    private void initGL() {
        glEnable(GL_TEXTURE_2D);
        glClearColor(0f, 0f, 0f, 0f);
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        glOrtho(0, Display.getWidth(), 0, Display.getHeight(), -1, 1);

    }

    public static void clear() {
        glClear(GL_COLOR_BUFFER_BIT);
    }


    public void render() {
        clear();


        glEnable(GL_TEXTURE_2D);

        glPushMatrix();
        glRotated(180, 1, 0, 0);
        glTranslatef(0, -(float) (map.getHeight() * map.getTileHeight()), 0f);
        // glRotated(180, 0, 1, 0);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //  int  y = initialMapPos+(int)entity.getY()-Display.getHeight()/5;

        //x = (int) player.getPlayerBody().getPosition().mul(30).x;

        for (int i = 0; i < map.getLayerCount(); i++) {
            map.render(0, 0, i);
        }

        glDisable(GL_BLEND);


        glPopMatrix();

        for (Entity entity : players) {


            if (entity instanceof Character) {

                if (((Character) entity).getWeapon().getTexture() == null) {
                    try {
                        FileInputStream weaponStream = new FileInputStream(new File("res/" + ((Character) entity).getWeapon().getTextureName()));
                        weaponTexture = TextureLoader.getTexture("PNG", weaponStream);
                        ((Character) entity).getWeapon().setTexture(weaponTexture);
                        weaponStream.close();
                        // Replace PNG with your file extension
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Display.destroy();
                        System.exit(1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Display.destroy();
                        System.exit(1);
                    }
                }


                if (((Character) entity).getName().equals(userName)) {


                    if (((Character) entity).getX() >= Display.getWidth() / 2 && ((Character) entity).getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                        lookAtX = entity.getX() - Display.getWidth() / 2;

                    } else if (((Character) entity).getX() <= (map.getWidth() * map.getTileWidth()) - Display.getWidth() / 2) {

                        lookAtX = 0;

                    } else {
                        lookAtX = (map.getWidth() * map.getTileWidth()) - Display.getWidth();

                    }


                    if (((Character) entity).getY() >= Display.getHeight() / 2 && ((Character) entity).getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {


                        lookAtY = ((Character) entity).getY() - Display.getHeight() / 2;

                    } else if (((Character) entity).getY() <= (map.getHeight() * map.getTileHeight()) - Display.getHeight() / 2) {

                        lookAtY = 0;

                    } else {
                        lookAtY = (map.getHeight() * map.getTileHeight()) - Display.getHeight();

                    }
                    glMatrixMode(GL_MODELVIEW);
                    glLoadIdentity();

                    GLU.gluLookAt(lookAtX, lookAtY, 1,
                            lookAtX, lookAtY, 0,
                            0, 1, 0);


                }


                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                if (!((Character) entity).isJumping()) {


                    if (((Character) entity).getState().equals("standing")) {
                        ((Character) entity).drawSteadySprite(entity.getX(), entity.getY(), entity.getDirection(), (long) delta);
                    } else if (((Character) entity).getState().equals("moving")) {

                        ((Character) entity).drawRunningSprite(entity.getX(), entity.getY(), entity.getDirection(), (long) delta);

                    }


                } else {


                    ((Character) entity).drawJumpSprite(entity.getX(), entity.getY(), entity.getDirection(), (long) delta);
                }

                ((Character) entity).drawShootSprite(entity.getX(), entity.getY(), entity.getDirection(), (long) delta);
                glDisable(GL_BLEND);


                glPopMatrix();





            }


        }


        glPushMatrix();

        glEnable(GL_TEXTURE_2D);
        glTexCoord2f(lookAtX + Mouse.getX(), lookAtY + Mouse.getY());
        glBindTexture(GL_TEXTURE_2D, mouseTexture.getTextureID());

        float mouseX = lookAtX + Mouse.getX();
        float mouseY = lookAtY + Mouse.getY();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBegin(GL_QUADS);

        glTexCoord2f(0, 0);
        glVertex2f(mouseX, mouseY);


        glTexCoord2f(1, 0);
        glVertex2f(mouseX + 30f, mouseY);


        glTexCoord2f(1, 1);
        glVertex2f(mouseX + 30f, mouseY + 30f);


        glTexCoord2f(0, 1);
        glVertex2f(mouseX, mouseY + 30f);


        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);

        glDisable(GL_BLEND);
        glPopMatrix();


        synchronized (lock) {
            glPushMatrix();
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            for (Weapon w : weapons) {

                w.drawSprite(w.getX(), w.getY(), w.getAngle(), 35f, 35f, 1);


            }
            glDisable(GL_BLEND);
            glPopMatrix();

        }


            synchronized (bulletLock) {

                for (VisualBullet bullet : bullets) {
                    if (normalBullet.getTexture() == null) {


                        try {
                            Texture t = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + "bullet" + ".png")));
                            normalBullet.setTexture(t);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    if (grenadeTextureBullet.getTexture() == null) {


                        try {
                            Texture t = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + "grenade" + ".png")));
                            grenadeTextureBullet.setTexture(t);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    if (grenadeShrapnel.getTexture() == null) {


                        try {
                            Texture t = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + "shrapnel" + ".png")));
                            grenadeShrapnel.setTexture(t);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                }

                glPushMatrix();
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                for (VisualBullet bullet : bullets) {


                    if (bullet.getShow().equals("true") && bullet.getType().equals("bullet")) {


                        float bulletX = bullet.getX();
                        float bulletY = bullet.getY();
                        glTexCoord2f(bulletX, bulletY);
                        glBindTexture(GL_TEXTURE_2D, normalBullet.getTexture().getTextureID());

                        glBegin(GL_TRIANGLES);
                        // first triangle, bottom left half
                        glTexCoord2f(0, 0);
                        glVertex2f(bulletX - 3f, bulletY - 3f);
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 3f, bulletY - 3f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 3f, bulletY + 3f / 2);

                        // second triangle, top right half
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 3f, bulletY - 3f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 3f, bulletY + 3f / 2);
                        glTexCoord2f(-1, -1);
                        glVertex2f(bulletX + 3f, bulletY + 3f / 2);


                        glEnd();

                    } else if (bullet.getShow().equals("true") && bullet.getType().equals("grenade")) {


                        float bulletX = bullet.getX();
                        float bulletY = bullet.getY();
                        glTexCoord2f(bulletX, bulletY);
                        glBindTexture(GL_TEXTURE_2D, grenadeTextureBullet.getTexture().getTextureID());

                        glBegin(GL_TRIANGLES);
                        // first triangle, bottom left half
                        glTexCoord2f(0, 0);
                        glVertex2f(bulletX - 10f, bulletY - 10f);
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 10f, bulletY - 10f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 10f, bulletY + 10f);

                        // second triangle, top right half
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 10f, bulletY - 10f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 10f, bulletY + 10f);
                        glTexCoord2f(-1, -1);
                        glVertex2f(bulletX + 10f, bulletY + 10f);


                        glEnd();


                    } else if (bullet.getShow().equals("true") && bullet.getType().equals("grenadeBullet")) {


                        float bulletX = bullet.getX();
                        float bulletY = bullet.getY();
                        glTexCoord2f(bulletX, bulletY);
                        glBindTexture(GL_TEXTURE_2D, grenadeShrapnel.getTexture().getTextureID());

                        glBegin(GL_TRIANGLES);
                        // first triangle, bottom left half
                        glTexCoord2f(0, 0);
                        glVertex2f(bulletX - 3f, bulletY - 3f);
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 3f, bulletY - 3f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 3f, bulletY + 3f);

                        // second triangle, top right half
                        glTexCoord2f(-1, 0);
                        glVertex2f(bulletX + 3f, bulletY - 3f);
                        glTexCoord2f(0, -1);
                        glVertex2f(bulletX - 3f, bulletY + 3f);
                        glTexCoord2f(-1, -1);
                        glVertex2f(bulletX + 3f, bulletY + 3f);


                        glEnd();


                    }
                }
                glDisable(GL_BLEND);
                glPopMatrix();
            }



                    synchronized (bloodLock) {

                            if (bloodSample.getTexture() == null) {

                                try {
                                    Texture t = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + "dot" + ".png")));
                                    bloodSample.setTexture(t);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }

                        glPushMatrix();
                        glBindTexture(GL_TEXTURE_2D, bloodSample.getTexture().getTextureID());
                        glEnable(GL_BLEND);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                        for (VisualBlood visualBloodl : blood) {

                            System.out.println("SIZE OF BLOOD " + blood.size());


                            if (visualBloodl.getShow().equals("true")) {


                                float visualBloodlX = visualBloodl.getX();
                                float visualBloodlY = visualBloodl.getY();
                                glTexCoord2f(visualBloodlX, visualBloodlY);

                                glBegin(GL_TRIANGLES);
                                // first triangle, bottom left half
                                glTexCoord2f(0, 0);
                                glVertex2f(visualBloodlX - visualBloodl.getRadius(), visualBloodlY - visualBloodl.getRadius());
                                glTexCoord2f(-1, 0);
                                glVertex2f(visualBloodlX + visualBloodl.getRadius(), visualBloodlY - visualBloodl.getRadius());
                                glTexCoord2f(0, -1);
                                glVertex2f(visualBloodlX - visualBloodl.getRadius(), visualBloodlY + visualBloodl.getRadius() / 2);

                                // second triangle, top right half
                                glTexCoord2f(-1, 0);
                                glVertex2f(visualBloodlX + visualBloodl.getRadius(), visualBloodlY - visualBloodl.getRadius());
                                glTexCoord2f(0, -1);
                                glVertex2f(visualBloodlX - visualBloodl.getRadius(), visualBloodlY + visualBloodl.getRadius() / 2);
                                glTexCoord2f(-1, -1);
                                glVertex2f(visualBloodlX + visualBloodl.getRadius(), visualBloodlY + visualBloodl.getRadius() / 2);


                                glEnd();




                            }
                        }
                        glDisable(GL_BLEND);
                        glPopMatrix();

                    }





        glDisable(GL_TEXTURE_2D);


        for (Entity entity : players) {


            glColor3f(1, 0, 0);

            glRectf(entity.getX() - 35f, entity.getY() - 70f - 2f, entity.getX() - 35f + 100f, entity.getY() - 70f + 2f);
            glFlush();


            glColor3f(0, 1f, 0);

            glRectf(entity.getX() - 35f, entity.getY() - 70f - 2f, entity.getX() - 35f + entity.getHealth(), entity.getY() - 70f + 2f);
            glColor4f(1f, 1f, 1f, 1f);
            glFlush();


            if (((Character) entity).getName().equals(userName)) {


                float numDiv = 15f;
                float sep = 0.04f;
                float barHeight = 1.0f / numDiv;

                glColor3f(0f, 0f, 0f);
                float c = entity.getY() - 80f;
                float r = entity.getX() - 35f;
                // i = row

                int totalCount = 0;
                //System.out.println("AMMO LEFT: "+ammo);
                for (float i = 0; i < ammo; i++) {

                    if (i >= ((Character) entity).getWeapon().getMaxAmmo() / 3) {
                        c -= 5;
                        r = entity.getX() - 35f;
                        i = 0;
                    }

                    glRectf(r - 1f, c + 2f, r + 1f, c - 2f);

                    r += 3;
                    totalCount++;

                    if (totalCount >= ammo) {

                        break;

                    }
                }
            }

        }


        synchronized (animationLock) {
            for (VisualExplosion animation : explosions) {


                if (!animation.isPlayed()) {
                    if (animation.getAnimation() == null) {
                        Animation ani = null;
                        try {
                            ani = new Animation(new SpriteSheet("res/explode22.png", 64, 64), 1);
                        } catch (SlickException e) {
                            e.printStackTrace();
                        }

                        animation.setAnimation(ani);
                        animation.getAnimation().setAutoUpdate(true);


                    }
                    glEnable(GL_BLEND);
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                    //animation.getAnimation().draw(animation.getX(), animation.getY());
                    if (animation.currentPic >= animation.totalPics) {

                        animation.setPlayed(true);

                    } else {
                        animation.getAnimation().draw(animation.getX() - 128, animation.getY() - 128, 256, 256);
                        animation.currentPic += 1f;
                    }


                    // animation.setPlayed(true);

                }
            }
        }

        if (showScore) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            float rectX = lookAtX+ 300;

            float rectX2 = lookAtX+Display.getWidth() - 300;

            float rectY = lookAtY+Display.getHeight() - 300;

            float rectY2 = lookAtY + 300;

            glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
            glRectf(rectX, rectY, rectX2, rectY2 );
            int xOffset = 0;
            int yOffset = 0;
//            System.out.println(" LONGEST STRING "+longestString + " rectX2-rectX" + (rectX2-rectX));
//
//            // STEP 1 : figure out longest string length to fit into scoreboard
//if(longestString==0) {
//    for (Entity entity : players) {
//
//
//        Character player = (Character) entity;
//        int tempStringLength = font.getWidth(entity.getStatusString());
//
//        //font.drawString(lookAtX + 320, (float) (map.getHeight() * map.getTileHeight()) - lookAtY - Display.getHeight() + 320 + yOffset, String.valueOf(entity.getStatusString()));
//
//
//        if (longestString == 0) {
//            longestString = tempStringLength;
//            longString = entity.getStatusString();
//        } else if (longestString < tempStringLength) {
//            longestString = tempStringLength;
//            longString = entity.getStatusString();
//        }
//        System.out.println(" LONGEST STRING " + longestString + " rectX2-rectX" + (rectX2 - rectX));
//    }
//}
//            // STEP 2: Calculate new font size to fit rectangle bounds according to the longest of the strings
//int currentFontSize = awtFont2.getSize();
//
//            int newFontSize =0;
//
//            if(longestString>=(rectX2-rectX-20)){
//
//                System.out.println(" LONGEST STRING "+longestString + " rectX2-rectX" + (rectX2-rectX));
//
//                    newFontSize = currentFontSize--;
//
//if(newFontSize<15){
//    newFontSize=15;
//    System.out.println(newFontSize +" NEW FONT SIZE IS 10!");
//}
//
//
//    // load a default java font
//    awtFont = new Font("Times New Roman", Font.BOLD, newFontSize);
//    font = new TrueTypeFont(awtFont, antiAlias);
//
//    // load font from file
//    try {
//        InputStream inputStream = ResourceLoader.getResourceAsStream("res/birdsFont.ttf");
//
//        awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
//        awtFont2 = awtFont2.deriveFont(newFontSize); // set font size
//        font2 = new TrueTypeFont(awtFont2, antiAlias);
//        inputStream.close();
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
//
//                int tempStringLength = font.getWidth(longString);
//
//
//
//                System.out.println(" LONGEST STRING "+longestString + " rectX2-rectX" + (rectX2-rectX));
//                    System.out.println(tempStringLength);
//
//
//
//
//
//            }

            // STEP 3: Draw

            for (Entity entity : players) {
                glPushMatrix();


                glRotated(180, 1, 0, 0);
                glTranslatef(0, -(float) (map.getHeight() * map.getTileHeight()), 0f);

                font.drawString(lookAtX + 320, (float) (map.getHeight() * map.getTileHeight()) - lookAtY - Display.getHeight() + 320 + yOffset, String.valueOf(entity.getStatusString()));

                yOffset += font.getHeight(entity.getStatusString())+20;
                glPopMatrix();
            }



            glDisable(GL_BLEND);

        }
    }

    public void doWork() {
        setupScreen(width, height);
        loop();
    }


    final Object killsLock = new Object();
    final Object deathsLock = new Object();
    final Object posXLock = new Object();
    final Object posYLock = new Object();
    final Object healthLock = new Object();
    final Object directionLock = new Object();
    final Object stateLock = new Object();
    final Object angleLock = new Object();
    public void run() {
        while (true) {
            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
        }
    }

    private Object lock = new Object();
    private boolean walking = false;

    private void parsePacket(final byte[] data, InetAddress address, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;

            case SOUND:
                packet = new Packet11PlaySound(data);

                Packet11PlaySound soundPacket = (Packet11PlaySound) packet;

                if (soundPacket.getName().equals("gunShot")) {

                    wavEffect.playAsSoundEffect(1f, 0.2f, false);


                }

                if (soundPacket.getName().equals("emptyClip")) {


                    emptyClipEffect.playAsSoundEffect(1f, 1f, false);


                }

                if (soundPacket.getName().equals("lowHealth")) {

                    if (!heartBeat.isPlaying()) {
                        heartBeat.playAsSoundEffect(1f, 1f, false);
                    }
                    System.out.println("Received low health sound effect");
                }


                if (soundPacket.getName().contains("walk") && walking && !feet.isPlaying()) {

                  //  feet.playAsSoundEffect(1f, 0.5f, false);


                }

                if (soundPacket.getName().equals("shellCasingStart")) {


                    if (!shellCasing.isPlaying()) {
                        shellCasing.playAsSoundEffect(1f, 1f, false);
                    }
                }

                if (soundPacket.getName().equals("shellCasingStop")) {


                    if (shellCasing.isPlaying()) {
                        shellCasing.stop();
                    }
                }

                if (soundPacket.getName().equals("bulletHit")) {

                if (bulletHit.isPlaying()) {
                    bulletHit.stop();
                }

                if (!bulletHit.isPlaying()) {
                    bulletHit.playAsSoundEffect(1f, 0.3f, false);
                }
                System.out.println("Hit!");
            }


                if (soundPacket.getName().equals("bulletHitDirt")) {



                    if (!bulletHitDirt.isPlaying()) {
                        bulletHitDirt.playAsSoundEffect(1f, 0.3f, false);
                    }
                    System.out.println("Hit Dirt!");
                }

                if (soundPacket.getName().equals("grenadeExplosion")) {



                    if (!grenadeExplosion.isPlaying()) {
                        grenadeExplosion.playAsSoundEffect(1f, 3f, false);
                    }
                    System.out.println("Grenade Explosion!");
                }


                if (soundPacket.getName().equals("grenadePinPull")) {



                    if (!grenadePin.isPlaying()) {
                        grenadePin.playAsSoundEffect(1f, 2f, false);
                    }
                    System.out.println("Grenade Pin Pulled!");
                }


                break;

            case LOGIN:
                packet = new Packet00Login(data);
                System.out.println("[" + address.getHostAddress() + ":" + port + "] " + ((Packet00Login) packet).getUsername() + " has connected...");


                boolean exists = false;
                for (Entity client : players) {
                    if (client instanceof Character) {
                        exists = ((Character) client).getName().equals(((Packet00Login) packet).getUsername());
                    }


                }
                int i = 0;
                if (!exists) {
                    System.out.println("ADDED CLIENT");
                    Character client = new Character(((Packet00Login) packet).getUsername(), 0, 0, 0, ((Packet00Login) packet).getHealth());


                    players.add(client);




                }
                break;

            case MOUSE:

                Packet14Mouse mouse = new Packet14Mouse(data);

                for(Entity entity : players){

                    Character player = (Character)entity;

                    if(player.getName().equals(mouse.getUsername())){

                        if(mouse.getMouseButton()==0){
                            synchronized (stateLock) {
                                player.setShooting(true);
                            }
                        }

                        if(mouse.getMouseButton()==1){
                            synchronized (stateLock) {
                                player.setShooting(false);
                            }
                        }

                        if(mouse.getMouseButton()==-1){
                            synchronized (stateLock) {
                                player.setShooting(false);
                            }
                        }

                    }

                }

                break;
            case MOVE:
                
                
             final Packet02Move  packet02Move = new Packet02Move(data);
                // System.out.println(((Packet02Move)packet).getY());

SwingUtilities.invokeLater(new Runnable() {
    @Override
    public void run() {
        
 
                for (Entity entity : players) {


                    if (packet02Move.getUsername().equals((entity).getName())) {

                        if (entity.getX() != packet02Move.getX() && packet02Move.getState().equals("moving")) {

                            walking = true;
                        } else {
                            walking = false;

                        }

                        synchronized (posXLock) {

                            (entity).setX(packet02Move.getX());
                        }
                        synchronized (posYLock) {

                            (entity).setY(packet02Move.getY());
                        }
                        synchronized (directionLock) {

                            (entity).setDirection(packet02Move.getDirection());
                        }

                        synchronized (healthLock) {

                            entity.setHealth(packet02Move.getHealth());

                        }
                        synchronized (killsLock) {
                            entity.setKills(packet02Move.getKills());
                        }
                        synchronized (deathsLock) {

                            entity.setDeaths(packet02Move.getDeaths());
                        }
                        ((Character)entity).setState(packet02Move.getState());


                        if(packet02Move.getState().equals("jumping")){
                            ((Character)entity).setJumping(true);
                        }

                        if(packet02Move.getState().equals("notjumping")){
                            ((Character)entity).setJumping(false);
                        }


                            if (packet02Move.getState().equals("standing")) {
                                ((Character) entity).setJumping(false);
                            }
                        

                       // System.out.println("Player: "+entity.getName()+"     Health: "+entity.getHealth()+"     Kills: "+entity.getKills()+"     Deaths:"+entity.getDeaths());
                    synchronized (statusStringLock) {

                        entity.setStatusString("Player: " + entity.getName() + "     Health: " + entity.getHealth() + "     Kills: " + entity.getKills() + "     Deaths:" + entity.getDeaths());
                    }
                    }



                }
    }
});

                break;

            case ANGLE:
                packet = new Packet03WeaponAngle(data);
                // System.out.println(((Packet02Move)packet).getY());

                for (Entity entity : players) {

                    if (((Packet03WeaponAngle) packet).getUsername().equals((entity).getName())) {

                        synchronized (angleLock) {
                            ((Character) entity).getWeapon().setAngle(((Packet03WeaponAngle) packet).getAngle());
                        }
                        break;
                    }

                }


                break;
            case ANIMATE:
                final Packet13Animate animate = new Packet13Animate(data);


                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {


                        // System.out.println(((Packet02Move)packet).getY());

                        synchronized (animationLock) {
                            if (animate.getName().equals("grenade")) {

                                explosions.add(new VisualExplosion(animate.getX() * 30f, animate.getY() * 30f, false));

                            }
                        }
                    };
                });
                
                
                break;
            case MESSAGE:

                packet = new Packet99ChatMessage(data);
                chatRoom.addMessage((Packet99ChatMessage) packet);

                break;
            case ITEM:

               final Packet04Item packet04Item = new Packet04Item(data);


                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {

                        boolean contains = false;

                synchronized (lock) {
                    Weapon weapon = new Weapon(packet04Item.getName(), packet04Item.getTextureName(), packet04Item.getX() * 30f, packet04Item.getY() * 30f, 0);
                    weapon.setId(packet04Item.getItemId());
                    for (Weapon w : weapons) {

                        if (w.getId().equals(weapon.getId())) {
                            contains = true;
                            w.setX(packet04Item.getX() * 30f);
                            w.setY(packet04Item.getY() * 30f);
                            break;
                        }

                    }

                    if (!contains) {

                        weapons.add(weapon);

                    }
                }
                    };
                });

                break;
            case ITEMREQUEST:

                packet = new Packet05RequestPickup(data);

                System.out.println("WAS OVER A WEAPON");



                synchronized (lock) {
                    Weapon weaponToRemove = null;
                    for (Weapon w : weapons) {

                        if (w.getId().equals(((Packet05RequestPickup) packet).getItemId())) {

                            System.out.println(((Packet05RequestPickup) packet).getItemId());
                            for (Entity player : players) {
                                if (((Packet05RequestPickup) packet).getUserName().equals(player.getName())) {
                                    ((Character) player).setWeapon(w);
                                    weaponToRemove = w;
                                    System.out.println(w.getTextureName());


                                    weaponTexture = w.getTexture();


                                    break;

                                }
                            }


                            break;
                        }
                    }



                if (weaponToRemove != null) {
                    synchronized (lock) {
                        removedWeapons.add(weaponToRemove);
                        weapons.remove(weaponToRemove);
                    }
                }
                }

                break;

            case BULLETPOSITION:

                packet = new Packet07BulletPosition(data);

                final Packet07BulletPosition pos = (Packet07BulletPosition) packet;

                SwingUtilities.invokeLater(new Runnable(){


                    @Override
                    public void run() {




                        synchronized (bulletLock) {

                        boolean containsBullet = false;
                        for (VisualBullet bullet : bullets) {


                            if (bullet.getBulletId().equals(pos.getBulletId())) {

                                bullet.setX(pos.getX() * 30f);
                                bullet.setY(pos.getY() * 30f);


                                bullet.setShow(pos.getShow());

                                containsBullet = true;
                            }

                        }

                        if (!containsBullet) {
                            VisualBullet bullet = new VisualBullet(pos.getBulletId(), pos.getX(), pos.getY(),pos.getShooter(),pos.getType());
                            for(Entity entity : players) {
                                if (entity instanceof Character) {
                                    Character character = (Character)entity;


                                    ((Character) entity).setShooting(true);




                                    if(userName.equals(pos.getShooter())){

                                        //System.out.println("RECEIVED BULLET POSITION "+pos.getCurrentAmmo());

                                        ammo = pos.getCurrentAmmo();
                                        //System.out.println("AMMO AFTER:  "+ character.getWeapon().getAmmo());

                                        break;
                                    }

                                }
                            }



                            bullets.add(bullet);

                        }
                    }
                    }
                });

                break;

            case BLOODPOSITION:

                packet = new Packet12BloodPosition(data);

                final Packet12BloodPosition posBlood = (Packet12BloodPosition) packet;

                SwingUtilities.invokeLater(new Runnable(){


                    @Override
                    public void run() {


                        // System.out.println("RECEIVED BLOOD POSITION");
                        synchronized (bloodLock) {



                            boolean containsBlood = false;
                            for (VisualBlood visualBlood : blood) {


                                if (visualBlood.getBloodId().equals(posBlood.getBloodId())) {

                                    visualBlood.setX(posBlood.getX() * 30f);
                                    visualBlood.setY(posBlood.getY() * 30f);
                                    visualBlood.setShow(posBlood.getShow());

                                    containsBlood = true;
                                }

                            }

                            if (!containsBlood) {

                                blood.add(new VisualBlood(posBlood.getBloodId(), posBlood.getX(), posBlood.getY(), posBlood.getWidth() * 30f));

                            }
                        }
                    }
                });

                break;

        }
    }

    long lastFpsTime = 1;
    private int fps = 30;

    public void loop() {

        long lastLoopTime = System.nanoTime();
        final int TARGET_FPS = 60;
        final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

        // keep looping round til the game ends
        while (true) {
            // work out how long its been since the last update, this
            // will be used to calculate how far the entities should
            // move this loop
            long now = System.nanoTime();
            long updateLength = now - lastLoopTime;
            lastLoopTime = now;
            double delta = updateLength / ((double) OPTIMAL_TIME);

            // update the frame counter
            lastFpsTime += updateLength;
            fps++;

            // update our FPS counter if a second has passed since
            // we last recorded
            if (lastFpsTime >= 1000000000) {
                System.out.println("(FPS: " + fps + ")");
                lastFpsTime = 0;
                fps = 0;
            }

            update(delta);
            render();


            // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);


            // we want each frame to take 10 milliseconds, to do this
            // we've recorded when we started the frame. We add 10 milliseconds
            // to this and then factor in the current time to give
            // us our final value to wait for
            // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
            try {
                long time = (lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000;

                if (time < 0) {
                    time *= -1;
                }
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }



public void setupPlayerSprites(){
    for(Entity player : players){
        if(player instanceof Character){
            try {
                if(((Character) player).getWeapon().getSprite()==null) {
                    SpriteSheet sprite = new SpriteSheet("res/" + ((Character) player).getWeapon().getTextureName(), 100, 100);
                    ((Character) player).getWeapon().setSprite(sprite);
                }
            } catch (SlickException e) {
                e.printStackTrace();
            }
        }


    }

}

}
