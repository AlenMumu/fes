package client;

/**
 * Created by alen on 6/21/14.
 */
public class MenuButton {

    private boolean clicked = false;
    private float x , y , width , height;
private String name;
    public MenuButton(String name, float x, float y, float width, float height) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }




    public boolean contains(float x , float y){


        if(this.x<=x && this.x+this.width>=x && this.y <= y && this.y+this.height>=y){
return true;
        }

        return false;

    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public String getName() {
        return name;
    }

    public boolean isClicked() {
        return clicked;
    }
}
