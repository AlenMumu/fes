package client;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.ResourceLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by alen on 6/20/14.
 */
public class Menu {

    private TiledMap backgroundImage;

    private float volume;
    private boolean soundOn;
private boolean mouseDown = false;
    private TrueTypeFont font;
    private TrueTypeFont font2;
    private boolean antiAlias = true;
    private ArrayList<MenuButton> buttons = new ArrayList<>();
    private ArrayList<String> songs = new ArrayList<>();
    private Audio music;

    private float offsetX , offsetY;


    public Menu(boolean soundOn, float volume) {
        super();

        this.soundOn = soundOn;
        this.volume = volume;




        songs.add("warAmbience1.wav");



    }

    public void initGl() {
        try {


            Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(910, 593));
            Display.create();


        } catch (LWJGLException e) {
            e.printStackTrace();
        }



        glEnable(GL_TEXTURE_2D);
        glClearColor(0f, 0f, 0f, 0f);
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        glOrtho(0, Display.getWidth(),0,  Display.getHeight(), -1, 1);

        try {
            backgroundImage = new TiledMap("res/levels/" + "menu.tmx");

            setupButtons();

        } catch (SlickException e) {
            e.printStackTrace();
        }
//        // load a default java font
//        java.awt.Font awtFont = new Font("Tahoma", Font.BOLD, 24);
//        font = new TrueTypeFont(awtFont, antiAlias);
//
//        // load font from file
//        try {
//            InputStream inputStream = ResourceLoader.getResourceAsStream("res/birdsFont.ttf");
//
//            Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
//            awtFont2 = awtFont2.deriveFont(24f); // set font size
//            font2 = new TrueTypeFont(awtFont2, antiAlias);
//            inputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    public static void clear() {
        glClear(GL_COLOR_BUFFER_BIT);
    }


    public void render() {
        clear();

        glEnable(GL_TEXTURE_2D);

        glPushMatrix();

        glRotated(180, 1, 0, 0);
        glTranslatef(0, -(float) (backgroundImage.getHeight() * backgroundImage.getTileHeight()), 0f);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //  int  y = initialMapPos+(int)entity.getY()-Display.getHeight()/5;

        //x = (int) player.getPlayerBody().getPosition().mul(30).x;


//       offsetX = Display.getWidth()/2-(backgroundImage.getWidth() * backgroundImage.getTileWidth())/2;
//offsetY = Display.getHeight()/2-(backgroundImage.getHeight() * backgroundImage.getTileHeight());
        offsetX = 0;
        offsetY = 0;
//
//        for (int i = 0; i < backgroundImage.getLayerCount(); i++) {
//            backgroundImage.render(Display.getWidth()/2-(backgroundImage.getWidth() * backgroundImage.getTileWidth())/2, Display.getHeight()/2-(backgroundImage.getHeight() * backgroundImage.getTileHeight()), i);
//        }

//        for (int i = 0; i < backgroundImage.getLayerCount(); i++) {
//            backgroundImage.render((int)offsetX, (int)offsetY, i);
//        }





        backgroundImage.render((int)offsetX, (int)offsetY, 0);

        for(MenuButton button : buttons){


            int i = buttons.indexOf(button)+1;
            if(button.contains(Mouse.getX(),Mouse.getY())){

                backgroundImage.render((int)offsetX, (int)offsetY, i);

            }

        }


        glPopMatrix();


//
//        glDisable(GL_TEXTURE_2D);
//
//
//        for(MenuButton button : buttons){
//
//            if(button.contains(Mouse.getX(),Mouse.getY())){
//
//                System.out.println(button.getName());
//
//            }
//            glPushMatrix();
//            glEnable(GL_BLEND);
//            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//
//            glColor3f(1,1,1);
//            glRectf(button.getX(),button.getY(),button.getX()+button.getWidth(),button.getY()+button.getHeight());
//            glDisable(GL_BLEND);
//
//            glPopMatrix();
//        }




    }


    long lastFpsTime = 1;
    private int fps = 30;


    public void update(double delta) {


        if(music!=null){

            if(!music.isPlaying()){

                try {
                    music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/" + songs.get(new Random().nextInt(songs.size() - 1))));
                    music.playAsMusic(1f,1f,true);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        }else{
            try {
                music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/warAmbience1.wav"));
                music.playAsMusic(volume,1f,true);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        userInput(delta);
        Display.update();
        Display.sync(60);

    }

    public void userInput(double delta){


        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
            System.exit(0);
        }

        for(MenuButton button : buttons){

            if(button.contains(Mouse.getX(),Mouse.getY())){

               if(Mouse.isButtonDown(0)&&!mouseDown){

                   if(button.getName().equals("joinRoom")){
                      ClientLauncher launcher1 =  new ClientLauncher();
                       Display.destroy();

                       launcher1.joinRoom();


                   }

                   if(button.getName().equals("exit")){


                       Display.destroy();

                       System.exit(0);

                   }


                   mouseDown = true;

               }else if(!Mouse.isButtonDown(0)&&!Mouse.isButtonDown(1)){

                   mouseDown=false;

               }

            }

        }

    }

    public void loop() {

        long lastLoopTime = System.nanoTime();
        final int TARGET_FPS = 60;
        final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

        // keep looping round til the game ends
        while (true) {
            // work out how long its been since the last update, this
            // will be used to calculate how far the entities should
            // move this loop
            long now = System.nanoTime();
            long updateLength = now - lastLoopTime;
            lastLoopTime = now;
            double delta = updateLength / ((double) OPTIMAL_TIME);

            // update the frame counter
            lastFpsTime += updateLength;
            fps++;

            // update our FPS counter if a second has passed since
            // we last recorded
            if (lastFpsTime >= 1000000000) {
                System.out.println("(FPS: " + fps + ")");
                lastFpsTime = 0;
                fps = 0;
            }

            update(delta);
            render();


            // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);


            // we want each frame to take 10 milliseconds, to do this
            // we've recorded when we started the frame. We add 10 milliseconds
            // to this and then factor in the current time to give
            // us our final value to wait for
            // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
            try {
                long time = (lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000;

                if (time < 0) {
                    time *= -1;
                }
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }



    public void setupButtons(){
       float objectGroupCount = backgroundImage.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = backgroundImage.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (backgroundImage.getObjectName(gi, oi).contains("button")) {
                    //  Platform platform = new Platform((float)map.getObjectX(gi,oi)/30f+(((float)map.getObjectWidth(gi,oi)/30f)/2),(((float)map.getHeight()*(float)map.getTileHeight())-((float)map.getObjectY(gi,oi)+map.getObjectHeight(gi,oi)))/30f,(((float)map.getObjectWidth(gi,oi)))/2,(float)map.getObjectHeight(gi,oi)/2,world,this);
                    MenuButton button = new MenuButton(backgroundImage.getObjectType(gi,oi),((float) backgroundImage.getObjectX(gi, oi)), (((float) backgroundImage.getHeight() * (float) backgroundImage.getTileHeight() - (float) backgroundImage.getObjectY(gi, oi))) - ((backgroundImage.getObjectHeight(gi, oi))),(float)backgroundImage.getObjectWidth(gi,oi),(float)backgroundImage.getObjectHeight(gi,oi));

                    buttons.add(button);
                }

            }
        }
    }
}
