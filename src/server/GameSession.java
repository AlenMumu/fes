package server;

import entities.*;
import org.jbox2d.common.Vec2;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import packets.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 * Created by amujkic on 23.04.2014.
 */
public class GameSession extends Thread{

private float roundTime = 60*10;
private float currentRoundTime = 0;
private float packageDropTimeSeconds = 30;
private boolean running = true;
    private boolean paused = false;
    private GameWorld world;
   private ArrayList<Grenade> grenadesToRemove = new ArrayList<>();
    private int windX = 0;
    private int windY = 0;
private List<GameClient> clients = Collections.synchronizedList(new ArrayList<GameClient>());

   final Object lock = new Object();

    private TiledMap map;
    private String mapName = "someFeetUnder.tmx";
    private ArrayList<Ladder> ladders = new ArrayList<>();
    private ArrayList<Platform> platforms = new ArrayList<>();
       private ArrayList<ServerWeapon> weapons = new ArrayList<>();
    private ArrayList<ServerItem> items = new ArrayList<>();

    private ArrayList<Vec2> spawnPoints = new ArrayList<>();
private int initialMapPos;
    private int fps = 30;
    private int frameCount = 0;
private int windStrengthFactor = 1;
    private ArrayList<Blood> blood = new ArrayList<>();
    private ArrayList<Bullet> bullets = new ArrayList<>();
    public GameSession(){

        try {
            Display.create();


setupWorld();

            Display.destroy();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }


    }

    public GameWorld getWorld(){
        return world;

    }




double packageDropTimer = 0;
    double lastFpsTime = 0;
    double heartBeat = 0;
    double footSteps = 0;
    double shellCasing = 0;
    double bloodTimer = 0;
    double bulletTimer = 0;
    double windTimer = 0;
    double windTimeSeconds = 0;
final Object bulletSync = new Object();
    final Object bloodSync = new Object();


    private boolean playing = true;
public void run(){



    long lastLoopTime = System.nanoTime();
    final int TARGET_FPS = 60;
    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

    // keep looping round til the game ends
    while (playing)
    {
        // work out how long its been since the last update, this
        // will be used to calculate how far the entities should
        // move this loop
        long now = System.nanoTime();
        long updateLength = now - lastLoopTime;
        lastLoopTime = now;
        double delta = updateLength / ((double)OPTIMAL_TIME);

        // update the frame counter
        lastFpsTime += updateLength;
        fps++;

        // update our FPS counter if a 10th of a second has passed since
        // we last recorded

        if (lastFpsTime >= 1000000000/10)
        {

            mouseDownTime++;
            heartBeat ++;
            footSteps ++;
            shellCasing ++;


        }

        if(roundTime>=currentRoundTime){
            playing = true;
        }else{

            playing = false;
        }



        // update our FPS counter if a second has passed since
        // we last recorded

        if (lastFpsTime >= 1000000000)
        {
            System.out.println("(FPS: "+fps+")");
            lastFpsTime = 0;
            fps = 0;


            for(GameClient client : clients) {
                // grenade timers


                for (Grenade g : client.entity.getGrenades()) {
if(g.isThrown()&&!g.isExploded()){
    synchronized (bulletSync) {

    if(g.getGrenadeTimer()>=g.getGrenadeSeconds()){

        g.explode();


                for(GameClient gameClient : clients) {
                gameClient.sendDataToClient(new Packet13Animate("grenade", 0, 0, 0, 0, g.getExplosionPoint().x, g.getExplosionPoint().y, 0), gameClient.getIpAddress(), gameClient.port);
                    gameClient.sendDataToClient(new Packet11PlaySound("grenadeExplosion"), gameClient.getIpAddress(), gameClient.port);

        }

        System.out.println(g.getGrenadeTimer());
        grenadesToRemove.add(g);

    }else {
        g.setGrenadeTimer(g.getGrenadeTimer() + 1);
        System.out.println(g.getGrenadeTimer());

    }
        System.out.println(g.getGrenadeTimer());

    }

}else{
    System.out.println(g.getGrenadeTimer());
}


                }

                synchronized (bulletSync) {

                    client.entity.getGrenades().removeAll(grenadesToRemove);
ArrayList<Grenade> grenadeToRemoveForever = new ArrayList<>();



                    for(Grenade grenade : grenadesToRemove){

                        if(grenade.getGrenadeTimer()>=grenade.getGrenadeExpirationTimer()){
                            System.out.println("Entered bullet loop");

                            for(Bullet bullet : grenade.getBullets()){

                                bullet.getBody().setActive(false);
                                bullet.getBody().setAwake(false);
                                world.destroyBody(bullet.getBody());

                                System.out.println("Destoryed bullet");

                            }

                            bullets.removeAll(grenade.getBullets());

                            grenadeToRemoveForever.add(grenade);
                        }else{
                            System.out.println("Incremented Timer");

                            grenade.setGrenadeTimer(grenade.getGrenadeTimer() + 1);

                        }


                    }

                    grenadesToRemove.removeAll(grenadeToRemoveForever);

                    grenadeToRemoveForever.clear();


                }

            }

            packageDropTimer++;
windTimer ++;
            bloodTimer++;
bulletTimer++;
            currentRoundTime++;
        }

        if(bloodTimer>=5){

    synchronized (bloodSync) {
        if(!blood.isEmpty()&&blood.size()>35){

        for (int i = 0; i < blood.size()-1; i++) {

            if(blood.get(i).getShow().equals("false")) {
                blood.get(i).getWorld().destroyBody(blood.get(i).getBody());
                blood.remove(i);
            }

        }

        bloodTimer = 0;

    }

}
        }



        if(bulletTimer>=5){
                synchronized (bulletSync) {
                    if(!bullets.isEmpty()&&bullets.size()>35){

                       // Random random = new Random();
                    for (int i = 0; i < bullets.size()-1; i++) {

                        if(bullets.get(i).getShow().equals("false")) {
                            bullets.get(i).getWorld().destroyBody(bullets.get(i).getBody());
                            bullets.remove(i);
                        }

                    }

                    bulletTimer = 0;

                }

            }
        }

        if(windTimer>=windTimeSeconds){
            Random random = new Random();


            int direction = random.nextInt(1);

            if(direction==1){

                windX = random.nextInt(windStrengthFactor);

                direction = random.nextInt(1);


                if(windX>0){
                    windX*=-1;
                }

                if(direction==1){

                    windY = random.nextInt(windStrengthFactor);

                    if(windY>0){
                        windY*=-1;
                    }
                }else{
                    windY = random.nextInt(windStrengthFactor);
                }


            }else{

                windX = random.nextInt(windStrengthFactor);

                direction = random.nextInt(1);

                if(direction==1){

                    windY = random.nextInt(windStrengthFactor);

                    if(windY>0){
                        windY*=-1;
                    }

                }else{
                    windY = random.nextInt(windStrengthFactor);
                }
            }


            windTimeSeconds = 5f + random.nextInt(55);

             windTimer=0;

        }

        //System.out.println("WIND CHANGES IN " + (windTimeSeconds-windTimer) + " and is currently moving " + windX + "x and " + windY+"y!");

        if(packageDropTimer>=packageDropTimeSeconds){
            Random random = new Random();
            synchronized (lock){


                WeaponItem item = new WeaponItem("gunCrate.png","gunCrate.png",getWorld(),(random.nextInt(map.getWidth()*map.getTileWidth()-30)+30)/30f,  ((300f + random.nextInt(500) / 30f) / 2),35f,35f);

                items.add(item);
            }
packageDropTimeSeconds = 5f + random.nextInt(55);


packageDropTimer=0;

        }
      //  System.out.println("PACKAGE DROPS IN : " + (packageDropTimeSeconds-packageDropTimer));
        world.step(1 / 60f, 8, 3);





    for(GameClient client:clients){

        for(GameClient c:clients) {

            // sending sound
            if (!c.isInAir() && !c.getTypeOfGround().isEmpty()) {
                if (footSteps >=30) {

                    Packet11PlaySound sound = new Packet11PlaySound(c.getTypeOfGround());
                    c.sendDataToClient(sound, c.getIpAddress(), c.port);


                }
            }

            if (c.entity.getHealth() < 10) {

                if(heartBeat>=30) {
                    Packet11PlaySound sound = new Packet11PlaySound("lowHealth");
                    c.sendDataToClient(sound, c.getIpAddress(), c.port);

                }

            }

            if (c.entity.getHealth() <= 0) {


                    c.deaths += 1;

                    c.resetPlayer();


                    for (GameClient killer : clients) {

                        if (c.getLastHitBullet().getShooter().equals(killer)) {

                            killer.kills += 1;
                            killer.resetPlayer();
                            break;
                        }

                    }


            }

            }

//if sounds have been sent reset their timers
        if (footSteps >=1) {

            footSteps=0;
        }

        if(heartBeat>=30) {

            heartBeat=0;
        }


        for(GameClient c:clients) {


            Packet02Move confirm = new Packet02Move(c.getUserName(),(int)c.getBody().getPosition().mul(30).x,(int)c.getBody().getPosition().mul(30).y,c.entity.getDirection(),c.entity.getHealth(),c.getCharacterState(),c.kills,c.deaths);

            Packet14Mouse mouse = new Packet14Mouse(c.getMouseButton(),c.getUserName());




//System.out.println("State of player at sending "+c.getCharacterState()+" "+c.getUserName());

            Packet03WeaponAngle weaponAngle = new Packet03WeaponAngle(c.getUserName(),c.getRevoluteJoint().getBodyA().getWorldCenter().mul(30f).x,c.getRevoluteJoint().getBodyA().getWorldCenter().mul(30f).y,c.entity.getWeapon().getAngle());

            client.sendDataToClient(mouse, client.getIpAddress(), client.port);
            client.sendDataToClient(confirm, client.getIpAddress(), client.port);
            client.sendDataToClient(weaponAngle, client.getIpAddress(), client.port);

        }

        synchronized (lock) {
            for (ServerWeapon w : weapons) {
                Packet04Item weapon = new Packet04Item(w.getName(), w.getTextureName(), "none", w.getWeaponBody().getPosition().x, w.getWeaponBody().getPosition().y, w.getWidth(), w.getHeight(), w.getId().toString());
                client.sendDataToClient(weapon, client.getIpAddress(), client.port);

            }
            for (ServerItem item : items) {
                if(item instanceof WeaponItem) {

                    WeaponItem w = (WeaponItem)item;
                            Packet04Item i = new Packet04Item(w.getName(), w.getTextureName(), "none", w.getWeapon().getWeaponBody().getPosition().x, w.getWeapon().getWeaponBody().getPosition().y, w.getWidth(), w.getHeight(), w.getId().toString());
                    client.sendDataToClient(i, client.getIpAddress(), client.port);
                }
            }
        }



        synchronized (bulletSync) {
            for (Bullet b : bullets) {


                client.sendDataToClient(new Packet07BulletPosition(b.getBulletId().toString(), b.getBody().getPosition().x, b.getBody().getPosition().y,b.getShow(),b.getShooter().getUserName(),b.getShooter().entity.getWeapon().getAmmo(),b.getType()), client.getIpAddress(), client.port);


                if(b.hitGround){




                    b.getBody().setAwake(false);
                    b.getBody().setActive(false);

                    if(b.getShow().equals("true")){
                        for(GameClient gameClient : clients){
                            gameClient.sendDataToClient(new Packet11PlaySound("bulletHitDirt"), gameClient.getIpAddress(), gameClient.port);

                        }
                    }

                    b.setShow("false");


                }

                if(b.hitPerson){




                    b.getBody().setAwake(false);
                    b.getBody().setActive(false);
                    b.setShow("false");


                    // DRAWING BLOOD SECTION

                    if(!b.drewBlood && b.getVictim()!=b.getShooter()) {
//                        for(int i =0;i<new Random().nextInt(10)+2;i++){
//
//                            addBlood(b.getDirection(), b.getAngle(), b.getBody().getPosition().x,b.getBody().getPosition().y, b.getVictim(), b);
//
//
//                        }

                        for(GameClient gameClient : clients){
                            gameClient.sendDataToClient(new Packet11PlaySound("bulletHit"), gameClient.getIpAddress(), gameClient.port);

                        }

                         b.drewBlood = true;
                    }




                }









            }
        }

        synchronized (bloodSync) {
            for (Blood b : blood) {



              //  System.out.println("SEND BLOOD");
                if(b.hitGround){
                    b.getBody().setActive(false);
                    b.getBody().setAwake(false);
                }

                if(b.hitPerson || b.hitWeapon){
                    b.getBody().setActive(false);
                    b.getBody().setAwake(false);
                   b.setShow("false");
                }

                client.sendDataToClient(new Packet12BloodPosition(b.getBloodId().toString(), b.getBody().getPosition().x, b.getBody().getPosition().y,b.getRadius()/2,b.getRadius()/2,b.getShow()), client.getIpAddress(), client.port);



            }
        }

        // System.out.println("SENDING PLAYER POSITION "+client.getUserName()+" "+(int)client.getBody().getPosition().mul(30).y);
    }

    // we want each frame to take 10 milliseconds, to do this
    // we've recorded when we started the frame. We add 10 milliseconds
    // to this and then factor in the current time to give
    // us our final value to wait for
    // remember this is in ms, whereas our lastLoopTime etc. vars are in ns.
    try {
        long time = (lastLoopTime-System.nanoTime() + OPTIMAL_TIME)/1000000;

        if(time<0){
            time*=-1;
        }
        Thread.sleep(time);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }

}


}



    public void setupWorld() {


        try {
            map = new TiledMap("res/levels/" + mapName);


        } catch (SlickException e) {
            e.printStackTrace();
        }
        world = new GameWorld(new Vec2(0,-9.8f),false);

        readXML();


        int objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                System.out.println(map.getObjectName(gi, oi));
                if (map.getObjectName(gi, oi).contains("player")) {

spawnPoints.add(new Vec2(((float) map.getObjectX(gi, oi) + 35f / 2) / 30f, (((float) map.getHeight() * (float) map.getTileHeight()) - (float) map.getObjectY(gi, oi) - (70f / 2)) / 30f));

                    System.out.println("ADDED SPAWN POINTS"+spawnPoints.get(0).x+" "+spawnPoints.get(0).y);

                }


                //System.out.println( map.getObjectProperty(gi, oi, "points", "" ) );
            }

        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {

                if (map.getObjectName(gi, oi).equals("origin")) {
                    initialMapPos = map.getHeight() * map.getTileHeight();
                }
            }
        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("land")) {
                    //  Platform platform = new Platform((float)map.getObjectX(gi,oi)/30f+(((float)map.getObjectWidth(gi,oi)/30f)/2),(((float)map.getHeight()*(float)map.getTileHeight())-((float)map.getObjectY(gi,oi)+map.getObjectHeight(gi,oi)))/30f,(((float)map.getObjectWidth(gi,oi)))/2,(float)map.getObjectHeight(gi,oi)/2,world,this);
                    Platform platform = new Platform(((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2), (float) map.getObjectWidth(gi, oi) / 2, (float) map.getObjectHeight(gi, oi) / 2, world);

                    System.out.println("MAP HEIGHT" + map.getHeight());
                    platforms.add(platform);
                }

            }
        }

        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("ladder")) {
                    Ladder ladder = new Ladder(((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2), (float) map.getObjectWidth(gi, oi) / 2, (float) map.getObjectHeight(gi, oi) / 2, world);

                    System.out.println("MAP HEIGHT" + map.getHeight());
                    ladders.add(ladder);
                }

            }
        }


        objectGroupCount = map.getObjectGroupCount();
        for (int gi = 0; gi < objectGroupCount; gi++) // gi = object group index
        {
            int objectCount = map.getObjectCount(gi);
            for (int oi = 0; oi < objectCount; oi++) // oi = object index
            {
                if (map.getObjectName(gi, oi).contains("gunCrate")) {


                    WeaponItem item = new WeaponItem(map.getObjectName(gi, oi),map.getObjectType(gi,oi),getWorld(),((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2),70f,35f);
//                    ServerWeapon serverWeapon = new ServerWeapon(map.getObjectName(gi, oi),map.getObjectType(gi,oi),getWorld(),((float) map.getObjectX(gi, oi) / 30f + (((float) map.getObjectWidth(gi, oi) / 30f)) / 2), (((float) map.getHeight() * (float) map.getTileHeight() - (float) map.getObjectY(gi, oi)) / 30f) - ((map.getObjectHeight(gi, oi) / 30f) / 2),70f,35f);

                    System.out.println("MAP HEIGHT" + map.getHeight());
              //      weapons.add(serverWeapon);
                    items.add(item);
                }

            }
        }

    }

    public void readXML() {
        try {

            File fXmlFile = new File("res/levels/" + mapName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("polygon");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
                    Element parent = (org.w3c.dom.Element) eElement.getParentNode();

                    System.out.println("POINTS: " + eElement.getAttribute("points"));
                    float xPos = Float.parseFloat(parent.getAttribute("x"));

                    float yPos = Float.parseFloat(parent.getAttribute("y"));
                    yPos = (map.getHeight() * map.getTileHeight()) - yPos / 30f;
                    Platform platform = new Platform(eElement.getAttribute("points"), xPos, yPos, world);
                    platforms.add(platform);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addClient(GameClient client){




            clients.add(client);



    }

    public List<GameClient> getClients() {
        return clients;
    }

    public void setClients(List<GameClient> clients) {
        this.clients = clients;
    }

    public ArrayList<Vec2> getSpawnPoints() {
        return spawnPoints;
    }

    public ArrayList<ServerWeapon> getWeapons() {
        return weapons;
    }

    public ArrayList<ServerItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<ServerItem> items) {
        this.items = items;
    }

    public void setWeapons(ArrayList<ServerWeapon> weapons) {
        this.weapons = weapons;
    }


   long mouseDownTime = 0;
    public void addBullet(final float direction, final float angle,float x, float y,GameClient shooter){

        synchronized (bulletSync) {





        if(mouseDownTime>=5){
            for(GameClient client:clients){




                    if(shooter.entity.getWeapon().getAmmo()>0){


                        Packet11PlaySound sound = new Packet11PlaySound("gunShot");
                        client.sendDataToClient(sound, client.getIpAddress(), client.port);

                        client.sendDataToClient(new Packet11PlaySound("shellCasingStop"), client.getIpAddress(), client.port);

                        client.sendDataToClient(new Packet11PlaySound("shellCasingStart"), client.getIpAddress(), client.port);


                    }else{

                        if(client == shooter){
                        Packet11PlaySound emtpyClipSound = new Packet11PlaySound("emptyClip");
                        client.sendDataToClient(emtpyClipSound, client.getIpAddress(), client.port);


                    }

                }
            }
          mouseDownTime = 0;


        }

if(shooter.entity.getWeapon().getAmmo()>0) {
    Bullet bullet = new Bullet(shooter, x - (shooter.entity.getDirection() * 30) / 30f, y, 0, world,"bullet");
    if (bullet.createBody()) {


        shooter.entity.getWeapon().setAmmo(shooter.entity.getWeapon().getAmmo()-1);
        bullets.add(bullet);

        double magnitude = 10000;


        Vec2 force = null;
        if (direction >= 1) {
            force = new Vec2((float) (magnitude * Math.cos(Math.toRadians(angle)))+windX, (float) (magnitude * Math.sin(Math.toRadians(angle)))+windY);

            bullet.getBody().applyLinearImpulse(force, new Vec2(x, y));
            // spriteSheet.getSprite(0,0).draw(bullet.getBulletBody().getWorldCenter().x,bullet.getBulletBody().getWorldCenter().y);

        } else {

            force = new Vec2((float) (-magnitude * Math.cos(Math.toRadians(-angle)))+windX, (float) (-magnitude * Math.sin(Math.toRadians(-angle)))+windY);


            bullet.getBody().applyLinearImpulse(force, new Vec2(x, y));

            //spriteSheet.getSprite(0,0).draw(bullet.getBulletBody().getWorldCenter().x, bullet.getBulletBody().getWorldCenter().y);
        }
    }
}

       }
    }


    public void addGrenade(final float direction, final float angle,float x, float y,GameClient shooter){

        synchronized (bulletSync) {

            ArrayList<Grenade> toRemove = new ArrayList<>();
            for(Grenade g :shooter.entity.getGrenades()){
                if(g.isExploded()){
                    toRemove.add(g);
                }

            }

shooter.entity.getGrenades().removeAll(toRemove);

            if(!shooter.entity.addGrenades(shooter,x - (shooter.entity.getDirection() * 60) / 30f, y+(60/30f),world)) {

                System.out.println("CREATING GRENADE FAILED");
                return;
            }


            System.out.println("CREATING GRENADE SUCCEEDED");


            for(GameClient gameClient : clients) {
                 gameClient.sendDataToClient(new Packet11PlaySound("grenadePinPull"), gameClient.getIpAddress(), gameClient.port);

            }

                for(Grenade grenade : shooter.entity.getGrenades()) {

                    if(!grenade.isThrown()){
                    for (Bullet bullet : grenade.getBullets()) {


                        bullets.add(bullet);

                        double magnitude = 5;


                        Vec2 force = null;
                        if (direction >= 1) {
                            force = new Vec2((float) (magnitude * Math.cos(Math.toRadians(angle))) + windX, (float) (magnitude * Math.sin(Math.toRadians(angle))) + windY);

                            bullet.getBody().applyLinearImpulse(force, new Vec2(x, y));
                            // spriteSheet.getSprite(0,0).draw(bullet.getBulletBody().getWorldCenter().x,bullet.getBulletBody().getWorldCenter().y);

                        } else {

                            force = new Vec2((float) (-magnitude * Math.cos(Math.toRadians(-angle))) + windX, (float) (-magnitude * Math.sin(Math.toRadians(-angle))) + windY);


                            bullet.getBody().applyLinearImpulse(force, new Vec2(x, y));

                            //spriteSheet.getSprite(0,0).draw(bullet.getBulletBody().getWorldCenter().x, bullet.getBulletBody().getWorldCenter().y);
                        }
                    }
                    grenade.setThrown(true);
                }
                }
        }
    }


    public void addBlood(final float direction, final float angle,float x, float y,GameClient victim,Bullet bullet){


     //   System.out.println("ENTERED CREATE BLOOD");

        synchronized (bloodSync) {



            Blood bloodParticle = new Blood(victim,x,y,angle,world);

                 if (bloodParticle.createBody()) {

                 //    System.out.println("CREATED BLOOD");

                    blood.add(bloodParticle);
                     Random random = new Random();




                     double magnitude = 3;

                     Vec2 force = new Vec2((float) (magnitude * Math.cos(Math.toRadians(random.nextInt(45))))+windX, (float) (magnitude * Math.sin(Math.toRadians(random.nextInt(45))))).mul(bullet.getShooter().entity.getDirection()+windY);


                     bloodParticle.getBody().applyForce(force, bloodParticle.getBody().getWorldCenter());



                }


        }
    }

    public ArrayList<Bullet> getBullets(){

        return bullets;
    }


    public int getWindX() {
        return windX;
    }

    public int getWindY() {
        return windY;
    }
}
