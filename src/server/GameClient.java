package server;

import entities.*;
import entities.Character;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.joints.RevoluteJoint;
import org.jbox2d.dynamics.joints.RevoluteJointDef;
import packets.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameClient extends Thread {


    private volatile boolean inAir = false;
    public volatile Character entity;
    private volatile InetAddress ipAddress;
    private volatile DatagramSocket socket;
    private volatile String userName;
    private volatile GameSession currentGame;
private volatile Bullet lastHitBullet;
    // WEAPON BODY
    private volatile int rateOfFire = 1;
    private volatile double angle;
    private volatile BodyDef weaponBodyDef;
    private volatile Body weaponBody;
    private volatile float densityW = 0.1f;
    private volatile float frictionW = 0.7f;
    private volatile String weaponTexture;
private volatile RevoluteJoint revoluteJoint;
    private volatile String state = "standing";
    // PLAYER BODY
    private volatile Body body;
    private volatile float density = 200f;
    private volatile float friction = 0.1f;
    private volatile FixtureDef boxFixture;
    private String typeOfGround = "";
    volatile int port;
private boolean shooting = false;
    volatile int kills = 0;
    volatile int deaths = 0;
private float mouseButton = -1;
   private volatile RevoluteJointDef revJoint;

    public GameClient(String userName, DatagramSocket socket, InetAddress address, int port, int spawnX, int spawnY) {

        this.userName = userName;

        this.socket = socket;
        this.ipAddress = address;

        this.port = port;

        this.entity = new Character(userName, spawnX * 30f, spawnY * 30f, 0, 100f);
        this.weaponTexture = this.entity.getWeapon().getTextureName();


    }

    @Override
    public void run() {
        while (true) {

            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {

                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
        }

    }

    private void parsePacket(byte[] data, InetAddress address, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;
            case MOVE:
                packet = new Packet02Move(data);
                //  System.out.println();
                if (((Packet02Move) packet).getUsername().equals(userName)) {


                    float moveDirX = ((Packet02Move) packet).getX();
                    float moveDirY = ((Packet02Move) packet).getY();

                    if(!inAir) {


                        if ((moveDirX > 0 || moveDirX < 0) && moveDirY > 0) {
                            state = "jumping";
                        } else if ((moveDirX == 0 && moveDirY == 0)) {

                            state = "standing";
                        } else if (moveDirX == 0 && moveDirY > 0) {

                            state = "jumping";
                        } else if (((moveDirX > 0 || moveDirX < 0) && moveDirY <= 0)) {

                            state = "moving";
                        }


                    }


                    if (moveDirX < 0) {


                        body.setLinearVelocity(new Vec2(-10f, body.getLinearVelocity().y));

                    }

                    if (moveDirX > 0) {

                        body.setLinearVelocity(new Vec2(10f, body.getLinearVelocity().y));

                    }

                    if (moveDirX == 0) {

                        body.setLinearVelocity(new Vec2(0, body.getLinearVelocity().y));

                    }

                    if (moveDirY > 0 && !inAir) {
                        body.setLinearVelocity(new Vec2(body.getLinearVelocity().x, 10));
                        inAir = true;
                        state = "jumping";
                    } else {

                        body.setLinearVelocity(new Vec2(body.getLinearVelocity().x, body.getLinearVelocity().y));

                    }






                  //  System.out.println(state +" of player " +userName);


                    entity.setX((int) body.getPosition().mul(30).x);
                    entity.setY((int) body.getPosition().mul(30).y);
                    entity.setDirection(((Packet02Move) packet).getDirection());
                    body.applyForce(new Vec2(currentGame.getWindX(),currentGame.getWindY()),body.getWorldCenter());

                }


                break;
            case ANGLE:
                packet = new Packet03WeaponAngle(data);
                //  System.out.println();
                if (((Packet03WeaponAngle) packet).getUsername().equals(entity.getWeapon().getName())) {
                    float deltaX = ((Packet03WeaponAngle) packet).getX();
                    float deltaY = ((Packet03WeaponAngle) packet).getY();
                    float angleInDegrees = (float) (Math.atan2(deltaY, deltaX) * 180 / 3.14);
                    //  System.out.println("ANGLE: " + angleInDegrees + "DELTA X " + deltaX + " DELTA Y" + deltaY);


                    entity.getWeapon().setAngle(angleInDegrees);

                }

                break;
            case MESSAGE:


                packet = new Packet99ChatMessage(data);
                System.out.println(((Packet99ChatMessage) packet).getUsername());
                for (GameClient client : currentGame.getClients()) {
                    for (GameClient c : currentGame.getClients()) {


                        client.sendDataToClient(((Packet99ChatMessage) packet), client.getIpAddress(), client.port);
                    }
                }
                break;

            case ITEMREQUEST:


                packet = new Packet05RequestPickup(data);
                ServerItem itemToRemove = null;
                for (ServerItem item : currentGame.getItems()) {

                    if (item instanceof WeaponItem) {


                        if (((WeaponItem) item).getWeapon().getWeaponBody().getPosition().mul(30).x - 5 < ((Packet05RequestPickup) packet).getX() && ((WeaponItem) item).getWeapon().getWeaponBody().getPosition().mul(30).x + 5 > ((Packet05RequestPickup) packet).getX()) {


                            Weapon weapon = new Weapon(userName, item.getTextureName(), 0, 0, 0, entity);
                            weapon.setId(item.getId().toString());
                            System.out.println(((Packet05RequestPickup) packet).getItemId());
                            entity.setWeapon(weapon);

                            Packet05RequestPickup i = new Packet05RequestPickup(0, 0, entity.getWeapon().getId(), ((Packet05RequestPickup) packet).getUserName());

                            for (GameClient client : currentGame.getClients()) {
                                for (GameClient c : currentGame.getClients()) {


                                    client.sendDataToClient(i, client.getIpAddress(), client.port);
                                }
                            }

                            itemToRemove = ((WeaponItem) item);

                            break;

                        }

                    }

                }

                if (itemToRemove != null) {
                    currentGame.getItems().remove(itemToRemove);
                }

                break;

            case SHOOT:


                packet = new Packet06Shoot(data);
                Packet06Shoot shoot = (Packet06Shoot) packet;
                if (shoot.getUserName().equals(userName)) {
                    if(shoot.getType().equals("weapon")) {


if(entity.getWeapon().automatic) {
    currentGame.addBullet(entity.getDirection(), entity.getWeapon().getAngle(), body.getPosition().x, body.getPosition().y, this);
}else{

if(entity.getWeapon().shoot){
    currentGame.addBullet(entity.getDirection(), entity.getWeapon().getAngle(), body.getPosition().x, body.getPosition().y, this);
    entity.getWeapon().shoot=false;
}

}


                    }else if(shoot.getType().equals("grenade")){


                        currentGame.addGrenade(entity.getDirection(), entity.getWeapon().getAngle(), body.getPosition().x, body.getPosition().y, this);

                    }
                    }


                System.out.println("RECEIVED SHOOT COMMAND");

                break;
            case MOUSE:


                Packet14Mouse mousePacket = new Packet14Mouse(data);

                if(mousePacket.getUsername().equals(userName)){

                    mouseButton = mousePacket.getMouseButton();

                }


                if(mouseButton==-1){
                    entity.getWeapon().shoot=true;
                }

                break;
        }
    }

    public void sendDataToClient(Packet packet, InetAddress ip, int port) {

        try {

            socket.send(new DatagramPacket(packet.getData(), packet.getData().length, ip, port));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setCurrentGame(GameSession gameSession) {
        System.out.println("GAME SET");
        currentGame = gameSession;


        boxFixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(entity.getX() / 30f, (entity.getY()) / 30f);
        boxDef.type = BodyType.DYNAMIC;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(35f / 30f, 35f / 30f);
        body = currentGame.getWorld().createBody(boxDef);
        body.setUserData(this);

        boxFixture.restitution = 0f;
        boxFixture.friction = friction;
        boxFixture.density = density;
        boxFixture.shape = boxShape;
        //boxFixture.filter.groupIndex = -5;
        body.createFixture(boxFixture);
        body.setFixedRotation(true);


        BodyDef weaponDef = new BodyDef();
        weaponDef.position.set((entity.getX()) / 30f, (entity.getY()) / 30f);
        weaponDef.type = BodyType.DYNAMIC;

        PolygonShape weaponShape = new PolygonShape();
        weaponShape.setAsBox((70f / 2) / 30f, (35f / 2) / 30f);

        weaponBody = currentGame.getWorld().createBody(weaponDef);

        FixtureDef weaponFixture = boxFixture;
        weaponFixture.restitution = 0f;
        weaponFixture.friction = frictionW;
        weaponFixture.density = densityW;
        weaponFixture.shape = weaponShape;
        //weaponFixture.filter.groupIndex = -3;
        weaponFixture.isSensor = true;


        weaponBody.createFixture(weaponFixture);
       revJoint = new RevoluteJointDef();
        revJoint.bodyA = body;
        revJoint.bodyB = weaponBody;
        revJoint.localAnchorA = new Vec2((this.body.getLocalCenter().mul(30f).x)/30f,this.body.getLocalCenter().mul(30f).y/30f);
        revJoint.localAnchorB = new Vec2((this.weaponBody.getLocalCenter().mul(30f).x)/30f,(this.weaponBody.getLocalCenter().mul(30f).y)/30f);
//        revJoint.localAnchorA = new Vec2(0, 0);
//        revJoint.localAnchorB = new Vec2(0, 0);

        revJoint.referenceAngle = 0;
        revJoint.enableLimit = true;


        revJoint.collideConnected = false;
        revoluteJoint = (RevoluteJoint) currentGame.getWorld().createJoint(revJoint);

        //assignedPlayer.getPlayerBody().createFixture(assignedPlayer.getBoxFixture());


        currentGame.addClient(this);


    }



    public synchronized void resetPlayer(){


            Random random = new Random();
            int index = random.nextInt(currentGame.getSpawnPoints().size());



           currentGame.getWorld().destroyJoint(revoluteJoint);
        currentGame.getWorld().destroyBody(body);
        currentGame.getWorld().destroyBody(weaponBody);


        this.entity = new Character(userName, currentGame.getSpawnPoints().get(index).x * 30f, currentGame.getSpawnPoints().get(index).y * 30f, 0, 100f);
        this.weaponTexture = this.entity.getWeapon().getTextureName();

        boxFixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(entity.getX() / 30f, (entity.getY()) / 30f);
        boxDef.type = BodyType.DYNAMIC;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(35f / 30f, 35f / 30f);
        body = currentGame.getWorld().createBody(boxDef);
        body.setUserData(this);

        boxFixture.restitution = 0f;
        boxFixture.friction = friction;
        boxFixture.density = density;
        boxFixture.shape = boxShape;
        //boxFixture.filter.groupIndex = -5;
        body.createFixture(boxFixture);
        body.setFixedRotation(true);


        BodyDef weaponDef = new BodyDef();
        weaponDef.position.set((entity.getX()) / 30f, (entity.getY()) / 30f);
        weaponDef.type = BodyType.DYNAMIC;

        PolygonShape weaponShape = new PolygonShape();
        weaponShape.setAsBox((70f / 2) / 30f, (35f / 2) / 30f);

        weaponBody = currentGame.getWorld().createBody(weaponDef);

        FixtureDef weaponFixture = boxFixture;
        weaponFixture.restitution = 0f;
        weaponFixture.friction = frictionW;
        weaponFixture.density = densityW;
        weaponFixture.shape = weaponShape;
        //weaponFixture.filter.groupIndex = -3;
        weaponFixture.isSensor = true;


        weaponBody.createFixture(weaponFixture);
        revJoint = new RevoluteJointDef();
        revJoint.bodyA = body;
        revJoint.bodyB = weaponBody;
        revJoint.localAnchorA = new Vec2((this.body.getLocalCenter().mul(30f).x)/30f,this.body.getLocalCenter().mul(30f).y/30f);
        revJoint.localAnchorB = new Vec2((this.weaponBody.getLocalCenter().mul(30f).x)/30f,(this.weaponBody.getLocalCenter().mul(30f).y)/30f);
//        revJoint.localAnchorA = new Vec2(0, 0);
//        revJoint.localAnchorB = new Vec2(0, 0);

        revJoint.referenceAngle = 0;
        revJoint.enableLimit = true;


        revJoint.collideConnected = false;
        revoluteJoint = (RevoluteJoint) currentGame.getWorld().createJoint(revJoint);



    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(InetAddress ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean exists(String userName) {
        if (userName.equals(this.userName)) {
            return true;
        }

        return false;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isInAir() {
        return inAir;
    }

    public void setInAir(boolean inAir) {
        this.inAir = inAir;
    }

    public String getTypeOfGround() {
        return typeOfGround;
    }

    public void setTypeOfGround(String typeOfGround) {
        this.typeOfGround = typeOfGround;
    }

    public String getCharacterState() {
        return state;
    }

    public void setCharacterState(String state) {
        this.state = state;
    }

    public GameSession getCurrentGame() {
        return currentGame;
    }

    public Body getWeaponBody() {
        return weaponBody;
    }

    public void setWeaponBody(Body weaponBody) {
        this.weaponBody = weaponBody;
    }

    public RevoluteJointDef getRevJoint() {
        return revJoint;
    }

    public void setRevJoint(RevoluteJointDef revJoint) {
        this.revJoint = revJoint;
    }

    public RevoluteJoint getRevoluteJoint() {
        return revoluteJoint;
    }

    public void setRevoluteJoint(RevoluteJoint revoluteJoint) {
        this.revoluteJoint = revoluteJoint;
    }


    public Bullet getLastHitBullet() {
        return lastHitBullet;
    }

    public void setLastHitBullet(Bullet lastHitBullet) {
        this.lastHitBullet = lastHitBullet;
    }

    public float getMouseButton() {
        return mouseButton;
    }

    public void setMouseButton(float mouseButton) {
        this.mouseButton = mouseButton;
    }
}
