package server;

import entities.*;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

/**
 * Created by alen on 6/7/14.
 */
public class CollisionManager implements ContactListener {
    @Override
    public void beginContact(Contact contact) {



if(contact.getFixtureB().getBody().getUserData() instanceof GameClient){


    if(contact.getFixtureA().getBody().isBullet()&&contact.getFixtureA().getBody().getUserData() instanceof Bullet){

Bullet bullet = (Bullet)contact.getFixtureA().getBody().getUserData();
        if(bullet.getShooter()!=(GameClient)contact.getFixtureB().getBody().getUserData()){

            GameClient client = (GameClient)contact.getFixtureB().getBody().getUserData();
           client.entity.setHealth(client.entity.getHealth()-bullet.damage);
            System.out.println(client.getUserName() +" "+client.entity.getHealth());

                bullet.setVictim(client);
                bullet.hitPerson = true;
client.setLastHitBullet(bullet);

        }
    }

    if(contact.getFixtureA().getBody().isBullet() &&contact.getFixtureA().getBody().getUserData() instanceof Object[]){


        Object[] obj = (Object[])contact.getFixtureA().getBody().getUserData();
        //get bullet
        Bullet bullet = (Bullet)obj[1];
        if(bullet.getShooter()!=(GameClient)contact.getFixtureB().getBody().getUserData()){

            GameClient client = (GameClient)contact.getFixtureB().getBody().getUserData();
            client.entity.setHealth(client.entity.getHealth()-bullet.damage);
            System.out.println(client.getUserName() + " " + client.entity.getHealth());

            bullet.setVictim(client);
            bullet.hitPerson = true;
            client.setLastHitBullet(bullet);

        }
    }


    if(contact.getFixtureA().getBody().getUserData() instanceof Platform){

        GameClient client = (GameClient)contact.getFixtureB().getBody().getUserData();

        client.setInAir(false);
        client.setCharacterState("notjumping");


        client.setTypeOfGround("walkDirt");


    }

    if(contact.getFixtureA().getBody().getUserData() instanceof ServerItem){

        GameClient client = (GameClient)contact.getFixtureB().getBody().getUserData();

        client.setInAir(false);
        client.setCharacterState("notjumping");


        client.setTypeOfGround("walkDirt");


    }


}

        if(contact.getFixtureA().getBody().getUserData() instanceof GameClient){




            if(contact.getFixtureB().getBody().isBullet() &&contact.getFixtureB().getBody().getUserData() instanceof Bullet){

                Bullet bullet = (Bullet)contact.getFixtureB().getBody().getUserData();
                if(bullet.getShooter()!=(GameClient)contact.getFixtureA().getBody().getUserData()){

                    GameClient client = (GameClient)contact.getFixtureA().getBody().getUserData();
                    client.entity.setHealth(client.entity.getHealth()-bullet.damage);
System.out.println(client.getUserName() + " " + client.entity.getHealth());

                    bullet.setVictim(client);
                    bullet.hitPerson = true;
                    client.setLastHitBullet(bullet);

                }
            }

            if(contact.getFixtureB().getBody().isBullet() &&contact.getFixtureB().getBody().getUserData() instanceof Object[]){


                Object[] obj = (Object[])contact.getFixtureB().getBody().getUserData();
                //get bullet
                Bullet bullet = (Bullet)obj[1];
                if(bullet.getShooter()!=(GameClient)contact.getFixtureA().getBody().getUserData()){

                    GameClient client = (GameClient)contact.getFixtureA().getBody().getUserData();
                    client.entity.setHealth(client.entity.getHealth()-bullet.damage);
                    System.out.println(client.getUserName() + " " + client.entity.getHealth());

                    bullet.setVictim(client);
                    bullet.hitPerson = true;
                    client.setLastHitBullet(bullet);

                }
            }

            if(contact.getFixtureB().getBody().getUserData() instanceof Platform){

                GameClient client = (GameClient)contact.getFixtureA().getBody().getUserData();

                client.setInAir(false);
client.setCharacterState("notjumping");
                client.setTypeOfGround("walkDirt");
            }

            if(contact.getFixtureB().getBody().getUserData() instanceof ServerItem){

                GameClient client = (GameClient)contact.getFixtureA().getBody().getUserData();

                client.setInAir(false);
                client.setCharacterState("notjumping");
                client.setTypeOfGround("walkDirt");
            }



        }


        if(contact.getFixtureA().getBody().isBullet()&&contact.getFixtureA().getBody().getUserData() instanceof Bullet){

            Bullet bullet = (Bullet)contact.getFixtureA().getBody().getUserData();

            if(contact.getFixtureB().getBody().getUserData() instanceof Platform){

                bullet.hitGround =true;



            }


        }




        if(contact.getFixtureB().getBody().isBullet()&&contact.getFixtureB().getBody().getUserData() instanceof Bullet){

            Bullet bullet = (Bullet)contact.getFixtureB().getBody().getUserData();

            if(contact.getFixtureA().getBody().getUserData() instanceof Platform){
                bullet.hitGround =true;

            }

        }

        if(contact.getFixtureB().getBody().isBullet()&&contact.getFixtureB().getBody().getUserData() instanceof Object[]){


            Object[] obj = (Object[])contact.getFixtureB().getBody().getUserData();
            //get bullet
            Bullet bullet = (Bullet)obj[1];


            if(contact.getFixtureA().getBody().getUserData() instanceof Platform){
                if(bullet.getBounceCount()>3&&bullet.exploded){
                    bullet.hitGround =true;
                }else{
                    bullet.incrementBounceCount();
                }

            }

        }


        if(contact.getFixtureA().getBody().isBullet()&&contact.getFixtureA().getBody().getUserData() instanceof Object[]){


            Object[] obj = (Object[])contact.getFixtureA().getBody().getUserData();
            //get bullet
            Bullet bullet = (Bullet)obj[1];


            if(contact.getFixtureB().getBody().getUserData() instanceof Platform){

                if(bullet.getBounceCount()>2&&bullet.exploded){
                    bullet.hitGround =true;
                }else{
                    bullet.incrementBounceCount();
                }


            }

        }


        if(contact.getFixtureA().getBody().getUserData() instanceof Blood){

            Blood blood = (Blood)contact.getFixtureA().getBody().getUserData();

            if(contact.getFixtureB().getBody().getUserData() instanceof GameClient){
                blood.hitPerson = true;
            }

            if(contact.getFixtureB().getBody().getUserData() instanceof ServerWeapon){
                blood.hitWeapon =true;
            }
            if(contact.getFixtureB().getBody().getUserData() instanceof Platform){
                blood.hitGround=true;
            }

        }

        if( contact.getFixtureB().getBody().getUserData() instanceof Blood){

            Blood blood = (Blood)contact.getFixtureB().getBody().getUserData();

            if(contact.getFixtureA().getBody().getUserData() instanceof GameClient){
               blood.hitPerson = true;
            }



            if(contact.getFixtureA().getBody().getUserData() instanceof ServerWeapon){
                blood.hitWeapon =true;
            }

            if(contact.getFixtureA().getBody().getUserData() instanceof Platform){
                blood.hitGround=true;
            }

        }



    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {

    }
}
