package server;

import entities.ServerWeapon;
import packets.Packet;
import packets.Packet00Login;
import packets.Packet04Item;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by amujkic on 23.04.2014.
 */
public class GameServer {



    public ArrayList<GameClient> clients = new ArrayList<>();

    private InetAddress ipAddress;
    private DatagramSocket socket;

    GameSession session = new GameSession();

    public GameServer(String ipAddress) {

        try {
            this.socket = new DatagramSocket(1331);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        acceptConnections();
    }

    public void acceptConnections(){

        while (true) {
            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {
               // System.out.println("Waiting for connections...");
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
        }

    }
    int i =0;
    public void parsePacket(byte[] data,InetAddress address, int port){

        String message = new String(data).trim();
        Packet.PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
        Packet packet = null;
        switch (type) {
            default:
            case INVALID:
                break;
            case LOGIN:
                packet = new Packet00Login(data);
                System.out.println("[" + address.getHostAddress() + ":" + port + "] "
                        + ((Packet00Login) packet).getUsername() + " has connected...");


                boolean exists = false;
                for(GameClient client : clients){

                    exists = client.exists(((Packet00Login) packet).getUsername());

                }

if(!exists) {
    System.out.println("ADDED CLIENT");

    Random random = new Random();
    int spawnIndex = random.nextInt(session.getSpawnPoints().size());
    GameClient client = new GameClient(((Packet00Login) packet).getUsername(),socket,address,port,(int)session.getSpawnPoints().get(spawnIndex).x,(int)session.getSpawnPoints().get(spawnIndex).y);

    clients.add(client);
    client.setCurrentGame(session);
    for(ServerWeapon w : session.getWeapons()){
        Packet04Item weapon = new Packet04Item(w.getName(),w.getTextureName(),"none",w.getWeaponBody().getPosition().x,w.getWeaponBody().getPosition().y,w.getWidth(),w.getHeight(),w.getId().toString());
        client.sendDataToClient(weapon, client.getIpAddress(), client.port);

    }
    if(i==1) {
        session.start();



    }else{
        i++;
    }

    for (GameClient client1 : clients) {
        client1.sendDataToClient(new Packet00Login(client.getUserName(), client.entity.getX(), client.entity.getY(),client.entity.getHealth()), client1.getIpAddress(), client1.port);
        client.sendDataToClient(new Packet00Login(client1.getUserName(), client1.entity.getX(), client1.entity.getY(),client1.entity.getHealth()), client.getIpAddress(), client.port);


    }
    client.start();
}



                break;



        }

    }
}
