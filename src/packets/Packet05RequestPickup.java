package packets;

/**
 * Created by amujkic on 30.04.2014.
 */
public class Packet05RequestPickup extends Packet {


    private float x, y;
private String itemId, userName;

    public Packet05RequestPickup(byte[] data) {
        super(05);
        String[] dataArray = readData(data).split(",");
        this.x = Float.parseFloat(dataArray[0]);
        this.y = Float.parseFloat(dataArray[1]);
this.itemId = dataArray[2];
        this.userName = dataArray[3];
    }

    public Packet05RequestPickup(float x, float y,String itemId, String userName) {
        super(05);
        this.x = x;
        this.y = y;
        this.itemId = itemId;
        this.userName = userName;

    }


    @Override
    public byte[] getData() {
        return ("05" + this.x + "," + this.y+","+this.itemId+","+this.userName).getBytes();

    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }



    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
