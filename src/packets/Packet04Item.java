package packets;

/**
 * Created by alen on 4/24/14.
 */
public class Packet04Item extends Packet {

    private String name;
    private String textureName;
    private String itemId;
    private float x;
    private float y;
    private float width;
    private float height;
    private String player = "none";

    public Packet04Item(byte[] data) {
        super(04);
        String[] dataArray = readData(data).split(",");
        this.name = dataArray[0];
        this.textureName = dataArray[1];
        this.player = dataArray[2];
        this.x = Float.parseFloat(dataArray[3]);
        this.y = Float.parseFloat(dataArray[4]);
        this.width = Float.parseFloat(dataArray[5]);
        this.height = Float.parseFloat(dataArray[6]);
        this.itemId = dataArray[7];
    }

    public Packet04Item(String name,String textureName,String player, float x, float y, float width, float height, String itemId) {
        super(04);
        this.name = name;
        this.textureName = textureName;
        this.player = player;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.itemId = itemId;

    }

    @Override
    public byte[] getData() {
        return  new String("04"+this.name+","+this.textureName+","+this.player+","+this.x+","+this.y+","+this.width+","+this.height+","+this.itemId).getBytes();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getTextureName() {
        return textureName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }
}
