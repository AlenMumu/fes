package packets;

/**
 * Created by amujkic on 30.04.2014.
 */
public class Packet12BloodPosition extends Packet {


private String bloodId,show;
private float x,y,width,height;

    public Packet12BloodPosition(byte[] data) {
        super(12);
        String[] dataArray = readData(data).split(",");

        this.bloodId = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.width = Float.parseFloat(dataArray[3]);
        this.height = Float.parseFloat(dataArray[4]);
        this.show = dataArray[5];
    }

    public Packet12BloodPosition(String bloodId, float x, float y,float width, float height, String show) {
        super(12);

        this.bloodId = bloodId;
this.x = x;
        this.y = y;
        this.show = show;
        this.width = width;
        this.height = height;

    }


    @Override
    public byte[] getData() {
        return ("12" + this.bloodId +","+this.x+","+this.y+","+this.width+","+this.height+","+this.show).getBytes();

    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
