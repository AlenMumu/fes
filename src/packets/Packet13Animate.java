package packets;

import org.newdawn.slick.Animation;

/**
 * Created by alen on 6/8/14.
 */
public class Packet13Animate extends Packet{

    private String name;
private float rows, columns, tw, th, x, y, totalImages;

    public Packet13Animate(byte[] data) {
        super(13);
        String[] dataArray = readData(data).split(",");

        this.name = dataArray[0];
        this.rows = Float.parseFloat(dataArray[1]);
        this.columns = Float.parseFloat(dataArray[2]);
        this.tw = Float.parseFloat(dataArray[3]);
        this.th = Float.parseFloat(dataArray[4]);
        this.x = Float.parseFloat(dataArray[5]);
        this.y = Float.parseFloat(dataArray[6]);
        this.totalImages = Float.parseFloat(dataArray[7]);


    }

    public Packet13Animate(String name, float rows, float columns, float tw, float th, float x, float y, float totalImages) {
        super(13);
        this.name = name;
        this.rows = rows;
        this.columns = columns;
        this.tw = tw;
        this.th = th;
        this.x = x;
        this.y = y;
        this.totalImages = totalImages;
    }



    @Override
    public byte[] getData() {
        return ("13" + this.name +","+ this.rows+","+ this.columns+","+ this.tw+","+ this.th+","+ this.x+","+ this.y+","+ this.totalImages).getBytes();

    }

    public String getName() {
        return name;
    }

    public void setName(String userName) {
        this.name = name;
    }

    public float getRows() {
        return rows;
    }

    public void setRows(float rows) {
        this.rows = rows;
    }

    public float getColumns() {
        return columns;
    }

    public void setColumns(float columns) {
        this.columns = columns;
    }

    public float getTw() {
        return tw;
    }

    public void setTw(float tw) {
        this.tw = tw;
    }

    public float getTh() {
        return th;
    }

    public void setTh(float th) {
        this.th = th;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getTotalImages() {
        return totalImages;
    }

    public void setTotalImages(float totalImages) {
        this.totalImages = totalImages;
    }
}
