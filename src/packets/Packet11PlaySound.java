package packets;

/**
 * Created by alen on 6/8/14.
 */
public class Packet11PlaySound extends Packet{

    private String name;

    public Packet11PlaySound(byte[] data) {
        super(11);
        String[] dataArray = readData(data).split(",");

        this.name = dataArray[0];
    }

    public Packet11PlaySound(String name) {
        super(11);

        this.name = name;

    }


    @Override
    public byte[] getData() {
        return ("11" + this.name).getBytes();

    }

    public String getName() {
        return name;
    }

    public void setName(String userName) {
        this.name = name;
    }

}
