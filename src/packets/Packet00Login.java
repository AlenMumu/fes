package packets;


public class Packet00Login extends Packet {

    private String username;
    private float x, y, health;

    public Packet00Login(byte[] data) {
        super(00);
        String[] dataArray = readData(data).split(",");
        this.username = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.health = Float.parseFloat(dataArray[3]);
    }

    public Packet00Login(String username, float x, float y, float health) {
        super(00);
        this.username = username;
        this.x = x;
        this.y = y;
        this.health = health;
    }


    @Override
    public byte[] getData() {
        return ("00" + this.username + "," + getX() + "," + getY()+","+this.health).getBytes();
    }

    public String getUsername() {
        return username;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }
}
