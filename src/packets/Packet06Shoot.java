package packets;

/**
 * Created by amujkic on 30.04.2014.
 */
public class Packet06Shoot extends Packet {


private String userName, type;

    public Packet06Shoot(byte[] data) {
        super(06);
        String[] dataArray = readData(data).split(",");

        this.userName = dataArray[0];
        this.type = dataArray[1];
    }

    public Packet06Shoot(String userName, String type) {
        super(06);

        this.userName = userName;
this.type = type;
    }


    @Override
    public byte[] getData() {
        return ("06" + this.userName+","+this.type).getBytes();

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
