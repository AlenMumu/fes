package packets;

/**
 * Created by amujkic on 30.04.2014.
 */
public class Packet07BulletPosition extends Packet {


private String bulletId,show,shooter,type;
private float x,y, currentAmmo;

    public Packet07BulletPosition(byte[] data) {
        super(07);
        String[] dataArray = readData(data).split(",");

        this.bulletId = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.show = dataArray[3];
        this.shooter = dataArray[4];
        this.currentAmmo = Float.parseFloat(dataArray[5]);
        this.type = dataArray[6];
    }

    public Packet07BulletPosition(String bulletId, float x, float y,String show,String shooter,float currentAmmo, String type) {
        super(07);

        this.bulletId = bulletId;
this.x = x;
        this.y = y;
        this.show = show;
this.shooter = shooter;
        this.currentAmmo = currentAmmo;
        this.type = type;
    }


    @Override
    public byte[] getData() {
        return ("07" + this.bulletId+","+this.x+","+this.y+","+this.show+","+this.shooter+","+this.currentAmmo+","+this.type).getBytes();

    }

    public String getBulletId() {
        return bulletId;
    }

    public void setBulletId(String bulletId) {
        this.bulletId = bulletId;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getShooter() {
        return shooter;
    }

    public void setShooter(String shooter) {
        this.shooter = shooter;
    }

    public float getCurrentAmmo() {
        return currentAmmo;
    }

    public void setCurrentAmmo(float currentAmmo) {
        this.currentAmmo = currentAmmo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
