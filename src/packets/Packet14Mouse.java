package packets;

/**
 * Created by alen on 6/19/14.
 */
public class Packet14Mouse extends Packet{

private String username;
    private float mouseButton;

    public Packet14Mouse(byte[] data) {
        super(14);
        String[] dataArray = readData(data).split(",");

       mouseButton = Float.parseFloat(dataArray[0]);
username = dataArray[1];

    }

    public Packet14Mouse(float mouseButton,String username) {
        super(14);
        this.username=username;
this.mouseButton = mouseButton;
    }



    @Override
    public byte[] getData() {
        return ("14"+ this.mouseButton+","+this.username).getBytes();

    }

    public float getMouseButton() {
        return mouseButton;
    }

    public void setMouseButton(float mouseButton) {
        this.mouseButton = mouseButton;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
