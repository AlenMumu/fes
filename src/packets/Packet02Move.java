package packets;


public class Packet02Move extends Packet {

    private String username, state;
    private float x, y, direction,health, kills, deaths;

    public Packet02Move(byte[] data) {
        super(02);
        String[] dataArray = readData(data).split(",");
        this.username = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.direction = Float.parseFloat(dataArray[3]);
    this.health = Float.parseFloat(dataArray[4]);
        this.state = dataArray[5];
        this.kills = Float.parseFloat(dataArray[6]);
        this.deaths = Float.parseFloat(dataArray[7]);
    }

    public Packet02Move(String username, float x, float y, float direction,float health,String state, float kills, float deaths) {
        super(02);
        this.username = username;
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.health = health;
        this.state = state;
        this.kills = kills;
        this.deaths = deaths;
    }


    @Override
    public byte[] getData() {
        return ("02" + this.username + "," + this.x + "," + this.y+","+this.direction+","+this.health+","+this.state+","+this.kills+","+this.deaths).getBytes();

    }

    public String getUsername() {
        return username;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
    public float getDirection() {
        return this.direction;
    }

    public float getHealth() {
        return health;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float getKills() {
        return kills;
    }

    public void setKills(float kills) {
        this.kills = kills;
    }

    public float getDeaths() {
        return deaths;
    }

    public void setDeaths(float deaths) {
        this.deaths = deaths;
    }
}
