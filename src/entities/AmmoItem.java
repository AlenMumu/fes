package entities;

import org.jbox2d.dynamics.World;

/**
 * Created by amujkic on 12.06.2014.
 */
public class AmmoItem extends ServerItem {

    private float ammo;
    public AmmoItem(String name, String textureName, World world, float x, float y, float width, float height) {
        super(name, textureName, world, x, y, width, height);
    }

    public float getAmmo() {
        return ammo;
    }

    public void setAmmo(float ammo) {
        this.ammo = ammo;
    }
}
