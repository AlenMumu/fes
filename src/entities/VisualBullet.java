package entities;

import org.newdawn.slick.opengl.Texture;

/**
 * Created by amujkic on 05.06.2014.
 */
public class VisualBullet {

    private String shooterName;
    private String bulletId;
    private float x,y;
    private Texture texture;
    private String show= "true";
    private String type;
    public VisualBullet(String bulletId, float x, float y, String shooterName, String type) {
        this.bulletId = bulletId;
        this.x = x;
        this.y = y;
        this.shooterName = shooterName;
        this.type =type;
    }

    public String getBulletId() {
        return bulletId;
    }

    public void setBulletId(String bulletId) {
        this.bulletId = bulletId;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
