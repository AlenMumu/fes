package entities;

import org.jbox2d.dynamics.World;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;
import server.GameClient;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by alen on 4/23/14.
 */
public class Character extends Entity {



 private Weapon weapon;
private Animation characterSprite;

    private Animation runningSprite;
    private Animation jumpingSprite;
    private Animation steadySprite;
    private Animation hitSprite;
    private Animation shootSprite;
    private boolean jumping = false;
    private boolean shooting = false;
private String state = "standing";
    private String shootingStatus = "notshooting";
    private ArrayList<VisualBullet> bullets = new ArrayList<>();
    private ArrayList<Grenade> grenades = new ArrayList<>();
private Audio feet;
    public Character(String name, float x, float y, float angle,float health) {
        super(name, x, y, angle, health);

        weapon = new Weapon(name,"mp5.png",0,0,0,this);



    }
    
    

    public synchronized void drawSprite(float x, float y,float direction, long delta){

if(characterSprite == null){

    try {
        characterSprite = new Animation(new SpriteSheet("res/spineboy-walk.png",142,256),1);

    } catch (SlickException e) {
        e.printStackTrace();
    }

}

        characterSprite.setSpeed(0.5f);
        characterSprite.update(delta);
characterSprite.draw(x-71f,y-138f);

    }

    public synchronized void drawRunningSprite(float x, float y,float direction, long delta){

        if(runningSprite == null){

            try {

                runningSprite = new Animation(new SpriteSheet("res/spineboy-run.png",126,200),2);

            } catch (SlickException e) {
                e.printStackTrace();
            }

        }

        runningSprite.setSpeed(0.7f);
        runningSprite.update(delta);


        if(!shooting) {
            //facing right
            if (direction < 0) {


                Image i = runningSprite.getCurrentFrame().getFlippedCopy(false, false);
                i.draw(x - 63f, y - 110f);
            } else {

//facing left

                Image i = runningSprite.getCurrentFrame().getFlippedCopy(true, false);


                i.draw(x - 63f, y - 110f);

            }

            if(runningSprite.getFrame()==2||runningSprite.getFrame()==8){
                if(feet == null) {
                    try {
                        feet = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sounds/footstepDirt.wav"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{

                    if(feet.isPlaying()){
                        feet.stop();
                    }
                    feet.playAsSoundEffect(1f,1f,false);

                }
            }

        }

    }

    public synchronized void drawSteadySprite(float x, float y,float direction, long delta){

        if(steadySprite == null){

            try {

                steadySprite = new Animation(new SpriteSheet("res/spineboy-idle.png",96,200),1);

            } catch (SlickException e) {
                e.printStackTrace();
            }

        }


        steadySprite.setSpeed(0.8f);

        if(!shooting) {
            //facing right
            if (direction < 0) {


                Image i = steadySprite.getCurrentFrame().getFlippedCopy(false, false);
                i.draw(x - 48, y - 110f);
            } else {

//facing left

                Image i = steadySprite.getCurrentFrame().getFlippedCopy(true, false);


                i.draw(x - 48, y - 110f);

            }


            steadySprite.update(delta);

        }

    }

    public synchronized void drawShootSprite(float x, float y,float direction, long delta){

        if(shootSprite == null){

            try {

                shootSprite = new Animation(new SpriteSheet("res/spineboy-shoot.png",151,200),2);
            } catch (SlickException e) {
                e.printStackTrace();
            }

        }


        shootSprite.setSpeed(0.8f);


if(shooting){

    //facing right
    if (direction < 0) {


        Image i = shootSprite.getCurrentFrame().getFlippedCopy(false, false);
        i.draw(x-30f, y - 80f);
    } else {

//facing left

        Image i = shootSprite.getCurrentFrame().getFlippedCopy(true, false);


        i.draw(x-90f , y - 80f);

    }




    System.out.println("UPDATING ANIMATION");
    shootSprite.update(delta);

}



        System.out.println(shooting);
        System.out.println(state);

    }

    public synchronized void drawJumpSprite(float x, float y,float direction, long delta){

        if(jumpingSprite == null){

            try {

                jumpingSprite = new Animation(new SpriteSheet("res/spineboy-jumpcopy.png",125,125),2);
            } catch (SlickException e) {
                e.printStackTrace();
            }

        }


        jumpingSprite.setSpeed(0.8f);
        jumpingSprite.stopAt(jumpingSprite.getFrameCount()-3);

        if(!shooting) {

            //facing right
            if (direction < 0) {


                Image i = jumpingSprite.getCurrentFrame().getFlippedCopy(false, false);
                i.draw(x - 62.5f, y - 62.5f);
            } else {

//facing left

                Image i = jumpingSprite.getCurrentFrame().getFlippedCopy(true, false);


                i.draw(x - 62.5f, y - 62.5f);

            }


            if (!jumpingSprite.isStopped()) {
                System.out.println("UPDATING ANIMATION");
                jumpingSprite.update(delta);

            }

        }

        System.out.println(jumping);
        System.out.println(state);

    }


    public synchronized void drawHitSprite(float x, float y,float direction, long delta){

        if(hitSprite == null){

            try {

                hitSprite = new Animation(new SpriteSheet("res/spineboy-hit.png",266,200),1);
            } catch (SlickException e) {
                e.printStackTrace();
            }

        }

        hitSprite.setSpeed(0.5f);


        //facing right
        if(direction<0){


            Image i = hitSprite.getCurrentFrame().getFlippedCopy(false, false);
            i.draw(x-133f,y-110f);
        }else{

//facing left

            Image i = hitSprite.getCurrentFrame().getFlippedCopy(true, false);


            i.draw(x-133f,y-110f);

        }

        hitSprite.update(delta);

    }

    public boolean addGrenades(GameClient shooter, float x, float y, World world){


        Grenade grenade = new Grenade();
        if(grenade.createBody(shooter,x,y,world)){
            grenades.add(grenade);
            return true;
        }

        return false;
    }

    public Animation getRunningSprite() {
        return runningSprite;
    }

    public synchronized void setRunningSprite(Animation runningSprite) {
        this.runningSprite = runningSprite;
    }

    public Animation getJumpingSprite() {
        return jumpingSprite;
    }

    public synchronized void setJumpingSprite(Animation jumpingSprite) {
        this.jumpingSprite = jumpingSprite;
    }

    public Animation getSteadySprite() {
        return steadySprite;
    }

    public synchronized void setSteadySprite(Animation steadySprite) {
        this.steadySprite = steadySprite;
    }

    public Animation getHitSprite() {
        return hitSprite;
    }

    public synchronized void setHitSprite(Animation hitSprite) {
        this.hitSprite = hitSprite;
    }

    public String getState() {
        return state;
    }

    public synchronized void setState(String state) {
        this.state = state;
    }

    public Animation getCharacterSprite() {
        return characterSprite;
    }

    public synchronized void setCharacterSprite(Animation characterSprite) {
        this.characterSprite = characterSprite;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public boolean isJumping() {
        return jumping;
    }

    public synchronized void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public synchronized void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public ArrayList<VisualBullet> getBullets() {
        return bullets;
    }

    public synchronized void setBullets(ArrayList<VisualBullet> bullets) {
        this.bullets = bullets;
    }


    public ArrayList<Grenade> getGrenades() {
        return grenades;
    }

    public synchronized void setGrenades(ArrayList<Grenade> grenades) {
        this.grenades = grenades;
    }

    public Animation getShootSprite() {
        return shootSprite;
    }

    public synchronized void setShootSprite(Animation shootSprite) {
        this.shootSprite = shootSprite;
    }

    public boolean isShooting() {
        return shooting;
    }

    public synchronized void setShooting(boolean shooting) {
        this.shooting = shooting;
    }

    public String getShootingStatus() {
        return shootingStatus;
    }

    public synchronized void setShootingStatus(String shootingStatus) {
        this.shootingStatus = shootingStatus;
    }
}
