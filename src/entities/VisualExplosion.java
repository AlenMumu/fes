package entities;

import org.newdawn.slick.Animation;

/**
 * Created by amujkic on 17.06.2014.
 */
public class VisualExplosion {

    private Animation animation;
    private float x,y;
    private boolean played = false;

    public VisualExplosion(float x, float y, boolean played) {

        this.x = x;
        this.y = y;
        this.played = played;
    }
public float totalPics = 48f;
public float currentPic = 1f;

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public boolean isPlayed() {
        return played;
    }

    public void setPlayed(boolean played) {
        this.played = played;
    }
}
