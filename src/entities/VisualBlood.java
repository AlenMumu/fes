package entities;

import org.newdawn.slick.opengl.Texture;

/**
 * Created by amujkic on 05.06.2014.
 */
public class VisualBlood {

    private String bloodId;
    private float x,y;
    private Texture texture;
    private String show= "true";
    private float radius;
    public VisualBlood(String bloodId, float x, float y, float radius) {
        this.bloodId = bloodId;
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public float getRadius() {
        return radius;
    }
}
