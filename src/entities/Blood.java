package entities;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.dynamics.*;
import server.GameClient;

import java.util.Random;
import java.util.UUID;

/**
 * Created by amujkic on 05.06.2014.
 */
public class Blood extends Entity {

private World world;

    // BULLET BODY
    public float radius = 0;
    private String show = "true";
    private Body body;
    private float density = 1f;
    private float friction = 0.5f;
    private UUID bloodId;
private GameClient victim;
   public int damage = 5;
public boolean hitGround = false;
    public boolean hitWeapon = false;
    public boolean hitPerson = false;
    public Blood(GameClient victim, float x, float y, float angle, final World world) {
        super(victim.getUserName(), x, y, angle,1);
        this.victim=victim;
        this.bloodId = UUID.randomUUID();
this.world = world;




    }

    public boolean createBody(){

        FixtureDef boxFixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(x, y );
        boxDef.type = BodyType.DYNAMIC;



       CircleShape circleShape = new CircleShape();

circleShape.m_radius = ((float)new Random().nextInt(3)+2f)/30f;
        radius = circleShape.m_radius;
        body = world.createBody(boxDef);



            boxFixture.friction = friction;
            boxFixture.density = density;
            boxFixture.shape = circleShape;
        boxFixture.filter.groupIndex = -5;

        boxFixture.restitution = 0f;
if(body!=null) {
    body.setUserData(this);
    body.createFixture(boxFixture);
    body.setFixedRotation(false);
    return true;
}
        return false;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }



    public UUID getBloodId() {
        return bloodId;
    }

    public void setBloodId(UUID bloodId) {
        this.bloodId = bloodId;
    }

    public GameClient getSVictim() {
        return victim;
    }

    public void setVictim(GameClient victim) {
        this.victim = victim;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public float getRadius() {
        return radius;
    }
}
