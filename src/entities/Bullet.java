package entities;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.*;
import org.newdawn.slick.opengl.Texture;
import server.GameClient;

import java.util.Random;
import java.util.UUID;

/**
 * Created by amujkic on 05.06.2014.
 */
public class Bullet extends Entity {

private World world;

    // BULLET BODY
    private int bounceCount=0;
    private String type;
    private String show = "true";
    private Body body;
    private float density = 1f;
    private float friction = 0.5f;
    private UUID bulletId;
private GameClient shooter;
    private GameClient victim;
   public int damage;
public boolean hitGround = false;
    public boolean hitPerson =false;
    public boolean drewBlood = false;
public boolean exploded = false;

    public Bullet(GameClient shooter, float x, float y, float angle, final World world, String type) {
        super(shooter.getUserName(), x, y, angle,1);
        this.shooter=shooter;
        this.bulletId = UUID.randomUUID();
this.world = world;

damage = new Random().nextInt(5)+3;

this.type = type;
    }

    public boolean createBody(){

        FixtureDef boxFixture = new FixtureDef();
        BodyDef boxDef = new BodyDef();
        boxDef.position.set(x, y );
        boxDef.type = BodyType.DYNAMIC;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(5f / 30f, 0.5f / 30f);
        body = world.createBody(boxDef);



        boxFixture.friction = friction;
        boxFixture.density = density;
        boxFixture.shape = boxShape;
        boxFixture.filter.groupIndex = -5;
        boxFixture.restitution = 0.00000000000000000000000000000001f;
        if(body!=null) {
            body.setUserData(this);
            body.setBullet(true);
            body.createFixture(boxFixture);
            body.setFixedRotation(true);
            return true;
        }
        return false;
    }



    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }



    public UUID getBulletId() {
        return bulletId;
    }

    public void setBulletId(UUID bulletId) {
        this.bulletId = bulletId;
    }

    public GameClient getShooter() {
        return shooter;
    }

    public void setShooter(GameClient shooter) {
        this.shooter = shooter;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public GameClient getVictim() {
        return victim;
    }

    public void setVictim(GameClient victim) {
        this.victim = victim;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void incrementBounceCount(){
        bounceCount++;

    }

    public int getBounceCount(){

        return  bounceCount;
    }
}
