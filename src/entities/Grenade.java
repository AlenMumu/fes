package entities;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import server.GameClient;

import java.util.ArrayList;

/**
 * Created by alen on 6/16/14.
 */
public class Grenade {


    private float blastPower = 100000;

private float grenadeTimer = 0;
    private float grenadeSeconds = 3;
    private float grenadeExpirationTimer = 30;
   private  boolean thrown = false;
    private boolean exploded = false;
    private Vec2 explosionPoint;
    private ArrayList<Bullet> bullets = new ArrayList<>();
    public Grenade() {


    }


    public boolean createBody(GameClient client,float x, float y, World world){


int count = 0;
        int numRays = 80;
        for (int i = 0; i < numRays; i++) {
            float angle = (float) Math.toDegrees((i / (float) numRays) * 360f);
            Vec2 rayDir = new Vec2((float) Math.sin(angle), (float) Math.cos(angle));

            BodyDef bd = new BodyDef();
            bd.type = BodyType.DYNAMIC;
            bd.fixedRotation = true; // rotation not necessary
            bd.bullet = true; // prevent tunneling at high speed
            bd.linearDamping = 0.6f; // drag due to moving through air
            bd.inertiaScale = 0; // ignore gravity

            bd.position = new Vec2(x, y); // start at blast center

            Body particleBody = world.createBody(bd);

            if(particleBody==null){

                for(Bullet b : bullets){
                    world.destroyBody(b.getBody());
                }
                bullets.clear();
                return false;
            }

            CircleShape circleShape = new CircleShape();
            circleShape.m_radius = 3f/30f; // very small

            FixtureDef fd = new FixtureDef();
            fd.shape = circleShape;
            fd.density = 300 / (float) numRays; // very high - shared across all particles
            fd.friction = 0; // friction not necessary
            fd.restitution = 0.99f; // high restitution to reflect off obstacles
            fd.filter.groupIndex = -1; // particles should not collide with each other
            particleBody.createFixture(fd);
particleBody.setBullet(true);
            Bullet b ;
            if(count==79){
                b = new Bullet(client, x, y, angle, world,"grenade");
            }else{

                b = new Bullet(client, x, y, angle, world,"grenadeBullet");
            }

            Object[] userInfo = new Object[]{rayDir,b};
            particleBody.setUserData(userInfo);
            b.setBody(particleBody);
            System.out.println(true);
            bullets.add(b);
count++;

        }

        return true;

    }

        public void explode(){



            System.out.println("Exploded");
        for(Bullet b : bullets){

            if(b.getBody()!=null) {

                explosionPoint = b.getBody().getWorldCenter();

                        Object[] obj = (Object[])b.getBody().getUserData();
                // index 0 = vec2
                //index 1 = bullet
                b.getBody().setLinearVelocity(((Vec2)obj[0]).mul(blastPower));
                if(b.getType().equals("grenade")){
                    b.setShow("false");
                }
                b.exploded = true;
            }

        }



    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public void setBullets(ArrayList<Bullet> bullets) {
        this.bullets = bullets;
    }

    public float getBlastPower() {
        return blastPower;
    }

    public void setBlastPower(float blastPower) {
        this.blastPower = blastPower;
    }

    public float getGrenadeTimer() {
        return grenadeTimer;
    }

    public void setGrenadeTimer(float grenadeTimer) {
        this.grenadeTimer = grenadeTimer;
    }

    public float getGrenadeSeconds() {
        return grenadeSeconds;
    }

    public void setGrenadeSeconds(float grenadeSeconds) {
        this.grenadeSeconds = grenadeSeconds;
    }

    public boolean isThrown() {
        return thrown;
    }

    public void setThrown(boolean thrown) {
        this.thrown = thrown;
    }

    public boolean isExploded() {
        return exploded;
    }

    public void setExploded(boolean exploded) {
        this.exploded = exploded;
    }

    public Vec2 getExplosionPoint() {
        return explosionPoint;
    }

    public void setExplosionPoint(Vec2 explosionPoint) {
        this.explosionPoint = explosionPoint;
    }

    public float getGrenadeExpirationTimer() {
        return grenadeExpirationTimer;
    }

    public void setGrenadeExpirationTimer(float grenadeExpirationTimer) {
        this.grenadeExpirationTimer = grenadeExpirationTimer;
    }
}
