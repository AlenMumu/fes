package entities;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.opengl.Texture;

/**
 * Created by alen on 4/23/14.
 */
public class Weapon extends Entity{
    private SpriteSheet sprite;

    private String textureName;
    private Character player;
private Texture texture;
    private float maxAmmo = 100;
    private float ammo = 100;
    public boolean automatic = false;
    public boolean shoot = true;
    public Weapon(String name,String textureName, float x, float y,float angle, Character player) {
        super(name, x, y, angle,1);

        this.textureName = textureName;
        this.player = player;



    }



    public Weapon(String name,String textureName, float x, float y,float angle) {
        super(name, x, y, angle,1);

        this.textureName = textureName;

    }

    public synchronized void drawSprite(float x, float y,float angle,float direction){

if(sprite == null){

    try {
        sprite = new SpriteSheet("res/" + textureName, 100, 100);
    } catch (SlickException e) {
        e.printStackTrace();
    }

}

        if(direction<0){

            angle*=-1;

            Image i = sprite.getFlippedCopy(false,false);
            i.setCenterOfRotation(65,65);
            i.setRotation(angle);
            i.draw(x - 65, y - 80, 130, 130);

        }else{


            Image i = sprite.getFlippedCopy(true,false);

            i.setCenterOfRotation(65,65);
            i.setRotation(angle);
            i.draw(x - 65, y - 80, 130, 130);

        }


    }

    public synchronized void drawSprite(float x, float y,float angle,float w, float h,float direction){

        if(sprite == null){

            try {
                sprite = new SpriteSheet("res/" + textureName, 100, 100);
            } catch (SlickException e) {
                e.printStackTrace();
            }

        }

        if(direction<0){

            angle*=-1;

            Image i = sprite.getFlippedCopy(false,false);
            i.setCenterOfRotation(w/2,h/2);
            i.setRotation(angle);
            i.draw(x - w/2, y - h/2, w, h);

        }else{


            Image i = sprite.getFlippedCopy(true,false);

            i.setCenterOfRotation(w/2,h/2);
            i.setRotation(angle);
            i.draw(x - w/2, y - h/2, w, h);

        }


    }


    public SpriteSheet getSprite() {
        return sprite;
    }

    public void setSprite(SpriteSheet sprite) {
        this.sprite = sprite;
    }


    public void setTexture(Texture t){
        texture = t;



    }

    public Texture getTexture() {
        return texture;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

    public void setPlayer(Character player) {
        this.player = player;
    }

    public Character getPlayer() {
        return player;
    }

    public float getMaxAmmo() {
        return maxAmmo;
    }

    public void setMaxAmmo(float maxAmmo) {
        this.maxAmmo = maxAmmo;
    }

    public float getAmmo() {
        return ammo;
    }

    public void setAmmo(float ammo) {
        this.ammo = ammo;
    }
}
