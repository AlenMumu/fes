package entities;

import org.jbox2d.dynamics.World;

/**
 * Created by amujkic on 12.06.2014.
 */
public class HealthItem extends ServerItem {

   private float health;

    public HealthItem(String name, String textureName, World world, float x, float y, float width, float height) {
        super(name, textureName, world, x, y, width, height);
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }
}
