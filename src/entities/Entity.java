package entities;

/**
 * Created by amujkic on 23.04.2014.
 */
public class Entity {

    private String statusString = "";
    private float health,kills,deaths;
    public String id;
    public float x, y, angle, direction;
private String name;
    public Entity(String name,float x, float y, float angle, float health) {
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.name = name;
        this.direction = 1;
this.health = health;

    }



    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getAngle() {
        return angle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    public float getKills() {
        return kills;
    }

    public void setKills(float kills) {
        this.kills = kills;
    }

    public float getDeaths() {
        return deaths;
    }

    public void setDeaths(float deaths) {
        this.deaths = deaths;
    }
}
