package entities;

import org.jbox2d.dynamics.World;

/**
 * Created by amujkic on 12.06.2014.
 */
public class WeaponItem extends ServerItem {

    private ServerWeapon weapon;
    public WeaponItem(String name, String textureName, World world, float x, float y, float width, float height) {
        super(name, textureName, world, x, y, width, height);

        weapon = new ServerWeapon(name, textureName, world, x, y, width, height);
    }

    public ServerWeapon getWeapon() {
        return weapon;
    }

    public void setWeapon(ServerWeapon weapon) {
        this.weapon = weapon;
    }
}
